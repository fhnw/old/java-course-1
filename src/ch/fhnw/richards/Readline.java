package ch.fhnw.richards;

import java.io.IOException;

public class Readline {

	public static void main(String[] args) throws IOException {
		String stringIn = readline();
		while (stringIn.length() > 0) {
			System.out.println("Line read: '" + stringIn + "'");
			stringIn = readline();
		}
	}

	// Refer to section 4.2 in the book
	public static String readline() throws IOException {
		boolean lineDone = false;
		String stringIn = "";
		
		while (!lineDone) {
			int c = System.in.read();
			lineDone = ( (c == -1) | (c == 13) ); // no data or 'cr'
			if (!lineDone) {
				if (c != 10) { // ignore 'lf'
					stringIn += (char) c;
				}
			}
		}
		return stringIn;
	}
	
}
