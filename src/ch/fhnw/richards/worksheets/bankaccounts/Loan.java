package ch.fhnw.richards.worksheets.bankaccounts;

public class Loan extends Account {

	public Loan(int ID, int custID) {
		super(ID, custID);
	}

	public boolean setBalance(double balance) {
		boolean success = false;
		if (balance <= 0) {
			setAccountBalance(balance);
			success = true;
		}
		return success;
	}
}
