package ch.fhnw.richards.worksheets.bankaccounts;

public abstract class Account {
	private int ID; // Account number
	private int customerID; // Customer
	
	private double balance; // positive for accounts, negative for loans
	private double interestRate; // annual interest rate

	public Account(int ID, int custID) {
		this.ID = ID;
		this.customerID = custID;
		balance = 0;
		interestRate = 0;
	}

	/**
	 * Interest is applied to accounts monthly. The field "interestRate" is the annual interest rate,
	 * where 2% interest is represented as 0.02. This method applies the interest for one month to
	 * the balance, i.e., 1/12 of the annual interest rate.
	 * 
	 * This method works identically both for BankAccount and for Loan objects.
	 */
	public void applyMonthlyInterest() {
		double monthlyInterestRate = interestRate / 12;
		balance = balance * (1 + monthlyInterestRate);
	}
	
	/**
	 * We allow subclasses to set the balance directly.
	 */
	protected void setAccountBalance(double balance) {
		this.balance = balance;
	}

	/**
	 * The public method that must be implemented by subclasses. This method checks the new
	 * balance for correctness: a Loan cannot have a positive balance, and a BankAccount
	 * cannot have a negative balance. This method returns a boolean value: true if the
	 * balance was successfully set, false if the balance could not be set.
	 */
	public abstract boolean setBalance(double balance);

	// --------------------------
	// Normal getters and setters
	// --------------------------
	
	public int getID() {
		return ID;
	}

	public int getCustomerID() {
		return customerID;
	}

	public double getBalance() {
		return balance;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
}
