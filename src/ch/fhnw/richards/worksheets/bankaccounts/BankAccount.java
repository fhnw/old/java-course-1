package ch.fhnw.richards.worksheets.bankaccounts;

/**
 * BankAccount is the parent class for accounts that have positive balances (i.e., not for loans).
 */
public abstract class BankAccount extends Account {

	public BankAccount(int ID, int custID) {
		super(ID, custID);
	}

	public boolean setBalance(double balance) {
		boolean success = false;
		if (balance >= 0) {
			setAccountBalance(balance);
			success = true;
		}
		return success;
	}
}
