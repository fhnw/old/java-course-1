package ch.fhnw.richards.worksheets.personnel;

/**
 * This is a test program for the addNewEmployee method
 */
public class personnelProgram {
	private Employee[] personnel;
	private static personnelProgram mainProgram;

	public static void main(String[] args) {
		mainProgram = new personnelProgram();

		mainProgram.addNewEmployee("Jones", "Mary");
		mainProgram.addNewEmployee("Smith", "John");
		mainProgram.addNewEmployee("Meier", "Susan");
		mainProgram.addNewEmployee("Bigler", "Alexander");
		mainProgram.addNewEmployee("Smith", "Sandy");

		mainProgram.printPersonnel();
	}

	public personnelProgram() {
		personnel = new Employee[100];
	}

	public void printPersonnel() {
		for (Employee e : personnel) {
			if (e != null) {
			System.out.println(e.toString());
			}
		}
	}

	/**
	 * This is the method that must be completed
	 */
	public void addNewEmployee(String lastName, String firstName) {
		Employee e = new Employee(lastName, firstName);

		// Insert the employee into the array
		int pos = 0;
		boolean inserted = false;
		while (!inserted) {
			if (personnel[pos] == null) {
				// inserting at the end of the array
				personnel[pos] = e;
				inserted = true;
			} else if (e.compareTo(personnel[pos]) > 0) {
				// new object is larger - move to the right in the array
				pos++;
			} else {
				// Insert the object here, moving all remaining objects to the right
				while (personnel[pos] != null) {
					Employee tmp = personnel[pos];
					personnel[pos] = e;
					e = tmp;
					pos++;
				}
				personnel[pos] = e;
				inserted=true;
			}
		}
	}
}
