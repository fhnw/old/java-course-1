package ch.fhnw.richards.worksheets.personnel;

public class Employee implements Comparable<Employee> {
	private static int nextID = 1;
	
	private int ID;
	private String lastName;
	private String firstName;
	
	public Employee(String lastName, String firstName) {
		this.ID = nextID++;
		this.lastName = lastName;
		this.firstName = firstName;
	}
	
	@Override
	public boolean equals(Object o) {
		boolean answer = false;
		if (o instanceof Employee) {
			Employee e = (Employee) o;
			answer = (ID == e.ID);
		}
		return answer;
	}

	@Override
	public int compareTo(Employee e) {
		int answer = lastName.compareTo(e.lastName);
		if (answer == 0) answer = firstName.compareTo(e.firstName);
		return answer;
	}
	
	@Override
	public String toString() {
		return lastName + ", " + firstName;
	}
}
