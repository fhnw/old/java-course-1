package ch.fhnw.richards;

public class IfExamples {

	public static void main(String[] args) {
		int i = 13;

		if (i > 5)
			i--;

		if (i > 5)
			i--;
		else
			i++;

		if (i > 5) {
			i *= 2;
			System.out.println(i);
		}

		if (i > 5) {
			i *= 3;
			System.out.println(i);
		} else {
			i -= 7;
			System.out.println(i);
		}

	}

	private void woof() {
		char c = 'c';
		switch (c) {
		case 'a':
			System.out.println("Found an a");
			break;
		case 'b':
			System.out.println("Found a b");
			break;
		case 'c':
			System.out.println("Found a c");
			break;
		case 'd':
			System.out.println("Found a d");
			break;
		case 'e':
			System.out.println("Found an e");
			break;
		case 'f':
			System.out.println("Found an f");
			break;
		default:
			
		}

	}

}
