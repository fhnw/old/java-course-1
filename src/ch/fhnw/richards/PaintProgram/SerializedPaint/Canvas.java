package ch.fhnw.richards.PaintProgram.SerializedPaint;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes.Line;
import ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes.Rectangle;
import ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes.Shape;

public class Canvas extends java.awt.Canvas implements MouseListener {
	private Point mousePress = null;
	private DrawingData data;
	
	public Canvas(DrawingData data) {
		super();
		this.data = data;
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(300, 300));
		this.addMouseListener(this);
	}
	
	@Override
	public void paint(Graphics g) {
		for (Shape shape : data.getShapes()) {
			shape.draw(g);
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		mousePress = evt.getPoint();
	}
	@Override
	public void mouseReleased(MouseEvent evt) {
		Point mouseUp = evt.getPoint();
		Color color = data.getCurrentColor();
		Graphics g = this.getGraphics();
		if (mousePress != null & g != null) {
			g.setColor(color);
			if (data.getCurrentTool() == null) {
				// do nothing
			} else if (data.getCurrentTool().equals(data.lineTool)) {
				data.addShape(new Line(mousePress, mouseUp, color));
				g.drawLine(mousePress.x, mousePress.y, mouseUp.x, mouseUp.y);
			} else if (data.getCurrentTool().equals(data.rectTool)) {
				data.addShape(new Rectangle(mousePress, mouseUp, color));
				int width = mouseUp.x-mousePress.x;
				int height = mouseUp.y - mousePress.y;
				g.drawRect(mousePress.x, mousePress.y, width, height);
			}
		}
	}
	@Override
	public void mouseClicked(MouseEvent evt) {
	}
	@Override
	public void mouseExited(MouseEvent evt) {
	}
	@Override
	public void mouseEntered(MouseEvent evt) {
	}
}
