package ch.fhnw.richards.PaintProgram.SerializedPaint;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Paint extends JFrame {
	// Program components
	private static Paint mainProgram;
	private static Canvas canvas;
	private static FileController fileController;
	private static DrawingData data;

	// Other GUI components
	private Box tools;
	protected final JButton btnSave;
	protected final JButton btnLoad;

	public static void main(String[] args) {
		if (mainProgram == null) {
			// Create program components
			data = new DrawingData();
			canvas = new Canvas(data);
			mainProgram = new Paint();
			fileController = new FileController(mainProgram, canvas, data);

			// Show GUI only when we are finished
			mainProgram.setVisible(true);
		}
	}

	private Paint() {

		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());

		// Center - the drawing canvas
		pane.add(canvas, BorderLayout.CENTER);

		// West - the drawing toolbox
		tools = Box.createVerticalBox();
		tools.add(new ColorPot(data, Color.RED));
		tools.add(new ColorPot(data, Color.BLUE));
		tools.add(new ColorPot(data, Color.GREEN));
		tools.add(new ColorPot(data, Color.WHITE));
		tools.add(new ColorPot(data, Color.BLACK));
		tools.add(Box.createVerticalStrut(10));
		tools.add(new DrawingTool(this, data, data.lineTool));
		tools.add(new DrawingTool(this, data, data.rectTool));
		tools.add(Box.createVerticalGlue());
		pane.add(tools, BorderLayout.WEST);

		// South - buttons for loading and saving
		Box buttons = Box.createHorizontalBox();
		buttons.add(Box.createHorizontalGlue());
		btnLoad = new JButton("Load");
		buttons.add(btnLoad);
		buttons.add(Box.createHorizontalStrut(10));
		btnSave = new JButton("Save");
		buttons.add(btnSave);
		pane.add(buttons, BorderLayout.SOUTH);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				endProgram();
			}
		});

		pack();
	}

	private void endProgram() {
		boolean okToClose = data.isSaved();

		if (!okToClose) {
			int answer = JOptionPane.showConfirmDialog(null,
					"Drawing has not been saved!\nReally close program?", "Close program?",
					JOptionPane.YES_NO_OPTION);
			okToClose = (answer == JOptionPane.YES_OPTION);
		}

		if (okToClose) {
			System.exit(0);
		}
	}
}
