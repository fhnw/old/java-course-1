package ch.fhnw.richards.PaintProgram.SerializedPaint;

import java.awt.Color;
import java.util.ArrayList;

import javax.swing.Icon;

import ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes.Shape;

/**
 * This class encapsulates the data for a drawing, as well as the
 * selections for color and tool that are currently in use.
 */
public class DrawingData {
	private Color currentColor;
	private Icon currentTool;
	private ArrayList<Shape> shapes;
	private boolean saved;

	protected final Icon lineTool;
	protected final Icon rectTool;
	
	protected DrawingData() {
		currentColor = Color.black;
		currentTool = null;
		
		// Create icons
		lineTool = createImageIcon(this.getClass(), "line_icon.gif");
		rectTool = createImageIcon(this.getClass(), "rectangle_icon.gif");
		
		// Initialize shapes
		shapes  = new ArrayList<Shape>();
		saved = true;
	}
	
	public Color getCurrentColor() {
		return currentColor;
	}
	public void setCurrentColor(Color currentColor) {
		this.currentColor = currentColor;
	}
	public Icon getCurrentTool() {
		return currentTool;
	}
	public void setCurrentTool(Icon currentTool) {
		this.currentTool = currentTool;
	}
	public ArrayList<Shape> getShapes() {
		return shapes;
	}
	public void setShapes(ArrayList<Shape> shapes) {
		this.shapes = shapes;
		saved = 
				true;
	}
	public void addShape(Shape newShape) {
		shapes.add(newShape);
		saved = false;
	}
	public boolean isSaved() {
		return saved;
	}
	public void setSaved() {
		saved = true;
	}
	
	/** Returns an ImageIcon, or null if the path was invalid.
	 * (adapted from http://java.sun.com/docs/books/tutorial/uiswing/misc/icon.html)
	 * 
	 * @param callingClass - the class associated with the image we are to convert to an icon
	 * @param path - the path to the image, relative to the class
	 * #param description - description of the icon (optional)
	*/
  public static javax.swing.ImageIcon createImageIcon(Class<?> callingClass, String path, String... description) {
    java.net.URL imgURL = callingClass.getResource(path);
    if (imgURL != null) {
    	if (description.length > 0)
        return new javax.swing.ImageIcon(imgURL, description[0]);
    	else
        return new javax.swing.ImageIcon(imgURL);
    } else {
        System.err.println("Couldn't find resource: " + path);
        return null;
    }
  }  	
}
