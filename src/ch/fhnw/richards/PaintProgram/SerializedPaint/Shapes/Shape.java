package ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.io.Serializable;

public abstract class Shape implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected Point start;
	protected Point end;
	protected Color color;

	protected Shape(Point start, Point end, Color color) {
		this.start = start;
		this.end = end;
		this.color = color;
	}
	
	public abstract void draw(Graphics g);
	
	public Point getStart() {
		return start;
	}
	
	public Point getEnd() {
		return end;
	}
	
	public Color getColor() {
		return color;
	}
}
