package ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Rectangle extends Shape {
	private static final long serialVersionUID = 1L;
	
	public Rectangle(Point start, Point end, Color color) {
		super(start, end, color);
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		int width = end.x - start.x;
		int height = end.y - start.y;
		g.drawRect(start.x, start.y, width, height);		
	}
}
