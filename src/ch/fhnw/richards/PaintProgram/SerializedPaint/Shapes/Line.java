package ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

public class Line extends Shape {
	private static final long serialVersionUID = 1L;
	
	public Line(Point start, Point end, Color color) {
		super(start, end, color);
	}

	@Override
	public void draw(Graphics g) {
		g.setColor(color);
		g.drawLine(start.x, start.y, end.x, end.y);
	}
}
