package ch.fhnw.richards.PaintProgram.SerializedPaint;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import ch.fhnw.richards.PaintProgram.SerializedPaint.Shapes.Shape;

public class FileController implements ActionListener {
	JButton btnLoad;
	JButton btnSave;
	DrawingData data;
	Canvas canvas;
	
	protected FileController(Paint mainProgram, Canvas canvas, DrawingData data) {
		this.canvas = canvas;
		
		this.btnLoad = mainProgram.btnLoad;
		btnLoad.addActionListener(this);
		
		this.btnSave = mainProgram.btnSave;
		btnSave.addActionListener(this);
	
		this.data = data;
	}

	@Override
	public void actionPerformed(ActionEvent event) {
		// Which button has been pressed?
		if (event.getSource() == btnLoad) {
			loadDrawing();
		} else if (event.getSource() == btnSave) {
			saveDrawing();
		}
	}
	
	private void saveDrawing() {
		JFileChooser fileDialog = new JFileChooser();
		fileDialog.showSaveDialog(null);
		File drawingFile = fileDialog.getSelectedFile();
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(drawingFile));
			ArrayList<Shape> shapes = data.getShapes();
			int numShapes = shapes.size(); 
			out.writeInt(numShapes);
			for (Shape shape : shapes) {
				out.writeObject(shape);
			}
			out.close();
			data.setSaved();
		} catch (Exception e) {
			displayException(e);
		}		
	}
	
	private void loadDrawing() {
		JFileChooser fileDialog = new JFileChooser();
		fileDialog.showOpenDialog(null);
		File drawingFile = fileDialog.getSelectedFile();
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(drawingFile));
			int numShapes = in.readInt();
			ArrayList<Shape> shapes = new ArrayList<Shape>();
			for (int i = 1; i <= numShapes; i++) {
				shapes.add((Shape) in.readObject());
			}
			in.close();
			data.setShapes(shapes);
			canvas.repaint();
		} catch (Exception e) {
			displayException(e);
		}		
	}
	
	private void displayException(Exception e) {
	 JOptionPane.showMessageDialog(null, e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
  }
}
