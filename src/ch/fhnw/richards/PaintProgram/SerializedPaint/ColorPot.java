package ch.fhnw.richards.PaintProgram.SerializedPaint;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ColorPot extends JButton implements ActionListener {
	DrawingData data;
	public ColorPot(DrawingData data, Color color) {
		super();
		this.data = data;
		this.setPreferredSize(new Dimension(20, 20));
		this.setBackground(color);
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		data.setCurrentColor(this.getBackground());
	}

}
