package ch.fhnw.richards;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class PerfectNumbers2 {

	public static void main(String[] args) {
		long trying = 1;
		long startTime = new Date().getTime();
		for (long num = 2; num <= Long.MAX_VALUE; num++) {
			// tell us what is happening
			if (num > trying) {
				long newTime = new Date().getTime();
				double elapsed = (newTime - startTime) / 1000.0;
				
				System.out.print("Numbers through " + trying);
				System.out.println(" - working for " + elapsed + " seconds");
				trying *= 2;
			}
			
			ArrayList<Long> primeFactors = primeFactors(num);
			ArrayList<Long> factors = factors(primeFactors);
			long sum = 0;
			for (long factor : factors) {
				sum += factor;
			}
			if (sum == num) {
				System.out.println(sum + " is perfect!");
			}
		}
	}

	private static ArrayList<Long> primeFactors(long num) {
		ArrayList<Long> factors = new ArrayList<Long>();
		while (num > 1) {
			long maxFactor = (long) Math.sqrt(num);
			boolean found = false;
			for (long i = 2; i <= maxFactor & !found; i++) {
				long div = num / i;
				if ((div * i) == num) {
					factors.add(i);
					num = div;
					found = true;
				}
			}
			if (!found) {
				factors.add(num);
				num = 1;
			}
		}
		return factors;
	}

	private static ArrayList<Long> factors(ArrayList<Long> primeFactors) {
		ArrayList<Long> factors = new ArrayList<Long>();
		factors.add((long) 1);
		for (int numFactors = 1; numFactors < primeFactors.size(); numFactors++) {
			int[] factorPositions = new int[numFactors];
			for (int i = 0; i < factorPositions.length; i++) {
				factorPositions[i] = i;
			}
			boolean isNextFactor = true;
			while (isNextFactor) {
				// calculate factor
				long newFactor = 1;
				for (int i = 0; i < factorPositions.length; i++) {
					newFactor *= primeFactors.get(factorPositions[i]);
				}

				// add factor only if not already present
				boolean found = false;
				for (Iterator<Long> i = factors.iterator(); i.hasNext() & !found;) {
					long value = i.next();
					if (value == newFactor) found = true;
				}
				if (!found) factors.add(newFactor);

				// try to increment factor positions
				isNextFactor = false;
				for (int i = factorPositions.length - 1; i >= 0 & !isNextFactor; i--) {
					int maxValue = primeFactors.size() + i - factorPositions.length;
					if (factorPositions[i] < maxValue) {
						// index not yet at max, so increment it
						factorPositions[i]++;

						// reset all following indices
						for (int j = i + 1; j < factorPositions.length; j++) {
							factorPositions[j] = factorPositions[j - 1] + 1;
						}

						// report success
						isNextFactor = true;
					}
				}

			}
		}
		return factors;
	}

	private static void printlist(ArrayList<Long> list) {
		for (Long l : list) {
			System.out.print(l + ", ");
		}
		System.out.println();
	}

}
