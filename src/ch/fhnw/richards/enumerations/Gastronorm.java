package ch.fhnw.richards.enumerations;

public class Gastronorm {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	Size size;
	Material material;
	
	public Gastronorm(Material material, Size size) {
		this.material = material;
		this.size = size;
	}
	
	public Material getMaterial() { return material; }
	public int getWidth() { return size.getWidth(); }
	public int getLength() { return size.getLength(); }

	public int getLiterVolume() { return size.getWidth() * size.getLength() / 10000; }

	// Enum Material
	public enum Material { Steel, Plastic };

	// Enum Size, all measurements in millimeters
	public enum Size {
		  GN1_4(163,265), GN1_2(265,325), GN1_1(325,530);
			private int width, length;
			Size(int width, int length) {
				this.width = width; this.length = length;
			}
			public int getWidth() { return width; }
			public int getLength() { return length; }
		};		
}
