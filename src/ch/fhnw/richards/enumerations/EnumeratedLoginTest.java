package ch.fhnw.richards.enumerations;


public class EnumeratedLoginTest {

	public static void main(String[] args) {
		EnumeratedLogin ralf = EnumeratedLogin.login(args[0], args[1]);
		System.out.println(args[0] + " has read-access? " + ralf.canRead());
		System.out.println(args[0] + " has write-access? " + ralf.canWrite());
	}
}
