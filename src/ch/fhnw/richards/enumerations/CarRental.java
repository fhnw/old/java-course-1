package ch.fhnw.richards.enumerations;

import java.util.Scanner;

/**
 * Read a series of car-rentals from the console. Check each rental against the list of available
 * cars. Give an error message if it is not possible. Otherwise, update the number available, and
 * update the total amount of outstanding rent.
 */
public class CarRental {
	/**
	 * Planning to change the attributes of enumerated values (here, the number available) is
	 * not good practice... See version 2 for a better solution
	 * @author brad
	 *
	 */
	public enum Cars {
		SMART(2, 100), RENAULT(4, 120), BMW(3, 200);

		private int numAvailable;
		private int costPerDay;

		Cars(int numAvailable, int costPerDay) {
			this.numAvailable = numAvailable;
			this.costPerDay = costPerDay;
		}
	}

	private static CarRental airportBranch;

	public static void main(String[] args) {
		airportBranch = new CarRental();
		airportBranch.readRentals();
	}

	/**
	 * Read from the console a series of car names. If the names exist, and cars are available, add up
	 * the rent due. Otherwise, give a sensible error message
	 */
	private void readRentals() {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter car to be rented: ");
		String nextCar = s.next();

		int totalRent = 0;

		while (!nextCar.equals("done")) {

			// Find car in our list
			Cars carFound = null;
			for (Cars car : Cars.values()) {
				if (car.toString().equalsIgnoreCase(nextCar)) {
					carFound = car;
				}
			}

			if (carFound == null) {
				System.out.println("No such car in inventory!");
			} else { // if cars are still available
				if (carFound.numAvailable <= 0) {
					System.out.println("No more " + nextCar + " cars available!");
				} else {
					carFound.numAvailable--;
					totalRent += carFound.costPerDay;
				}

			}
			System.out.println("Enter car to be rented: ");
			nextCar = s.next();
		}
		System.out.println("Total rent due: " + totalRent);
	}
}
