package ch.fhnw.richards.enumerations;

import java.util.EnumSet;

public enum EnumeratedLogin {
	INVALID_USER,
	GUEST,
	VALID_USER,
	ADMINISTRATOR;

	public boolean canRead() {
		return (this.compareTo(GUEST) >= 0);
	}

	public boolean canWrite() {
		return (this.compareTo(VALID_USER) >= 0);
	}

	public static EnumeratedLogin login(String user, String password) {
		if ("guest".equals(user)) {
			return GUEST;
		} else if (validateAdministrator(user, password)) {
			return ADMINISTRATOR;
		} else if (validateUser(user, password)) {
			return VALID_USER;
		} else {
			return INVALID_USER;
		}
	}
	
	private static boolean validateUser(String user, String password) {
		return "Ralf".equals(user);
	}
	private static boolean validateAdministrator(String user, String password) {
		return "Super".equals(user);
	}
}
