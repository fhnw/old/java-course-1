package ch.fhnw.richards.applet1;

import java.awt.BorderLayout;
import javax.swing.JApplet;
import javax.swing.JLabel;

public class HelloWorldApplet extends JApplet {
	public HelloWorldApplet() {
		this.setLayout(new BorderLayout());
		JLabel hi = new JLabel("Hello, World!");
		this.add(hi, BorderLayout.CENTER);
	}
}
