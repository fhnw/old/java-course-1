package ch.fhnw.richards.lecture01;

public class FloatingPointTest {

	public static void main(String[] args) {
		float A = 1000000;
		float B = (A+1) / 1000;
		float C = A / 1000;
		float D = B - C;
		float E = 1000000 * D;
		float F = E - 1000;
		System.out.println(F);
	}
}
