package ch.fhnw.richards;

public class PortTester {
	private String ipAddress;
	private int portNumber;
	
	public PortTester(String ipAddress, int portNumber) {
		this.ipAddress = ipAddress;
		this.portNumber = portNumber;
	}

	/** Test this port to see if it is secure.
	 * @return true if the port is secure, false
	 * if it is open
	 */
	public boolean test() {
		try {
			// Try to connect
			java.net.Socket sock = new java.net.Socket(ipAddress, portNumber);
			// Close the connection
			sock.close();
			// Port open (vulnerability)
			return false;
		} catch (Exception e) {
			// Failure = good news - port secure
			return true;
		}
	}
}
