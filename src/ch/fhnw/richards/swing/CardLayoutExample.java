package ch.fhnw.richards.swing;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class CardLayoutExample extends JFrame implements ActionListener {
	JButton btnA;
	JButton btnB;
	JButton btnC;
	JPanel centerPanel;

	public static void main(String[] args) {
		new CardLayoutExample();
	}
	
	private CardLayoutExample() {
		this.setLayout(new BorderLayout());
		
		Box buttons = Box.createHorizontalBox();
		btnA = new JButton("A");
		btnB = new JButton("B");
		btnC = new JButton("C");
		buttons.add(Box.createHorizontalGlue());
		buttons.add(btnA);
		buttons.add(Box.createHorizontalStrut(5));
		buttons.add(btnB);
		buttons.add(Box.createHorizontalStrut(5));
		buttons.add(btnC);
		buttons.add(Box.createHorizontalGlue());
		
		btnA.addActionListener(this);
		btnB.addActionListener(this);
		btnC.addActionListener(this);
		
		// Center area
		centerPanel = new JPanel();
		
		// Set up card layout
		createCardLayout();
		
		// Add all to JFrame
		this.add(buttons, BorderLayout.NORTH);
		this.add(centerPanel, BorderLayout.CENTER);
		
		pack();
		setVisible(true);
	}

	private void createCardLayout() {
		centerPanel.setLayout(new CardLayout());
		
		// Create three cards
		for (char c = 'A'; c <= 'C'; c++) {
			String name = Character.toString(c);
			JPanel card = new JPanel();
			card.setLayout(new BorderLayout());
			card.add(new JLabel(name), BorderLayout.CENTER);
			card.setPreferredSize(new Dimension(300,300));
			centerPanel.add(card, name);
		}
	}

	@Override
	public void actionPerformed(ActionEvent evt) {
		JButton evtButton = (JButton) evt.getSource();
		
		CardLayout cardLayout = (CardLayout) centerPanel.getLayout();
		cardLayout.show(centerPanel, evtButton.getText());
	}
}
