package ch.fhnw.richards.swing;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class ButtonClick2 extends JFrame {
	private JLabel lblNumber;
	private JButton btnClick;

	public static void main(String args[]) {
		new ButtonClick2();
	}

	public ButtonClick2() {
		super("Button Click 2");
		getContentPane().setLayout(new GridLayout(2, 1));

		lblNumber = new JLabel();
		lblNumber.setText("0");
		getContentPane().add(lblNumber);

		btnClick = new JButton();
		btnClick.setText("Click Me!");
		getContentPane().add(btnClick);
		btnClick.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int newValue;

				newValue = Integer.parseInt(lblNumber.getText()) + 1;
				lblNumber.setText(Integer.toString(newValue));
			}			
		});

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0); // JVM beenden
			}
		});

		pack();
		setVisible(true);
	}
}
