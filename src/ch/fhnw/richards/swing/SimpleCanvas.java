package ch.fhnw.richards.swing;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class SimpleCanvas extends JFrame {
	private Canvas canvas;

	public static void main(String[] args) {
		new SimpleCanvas();
	}

	public SimpleCanvas() {
		super("Simple Canvas");
    this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		this.setLayout(new BorderLayout());
		
		canvas = new Canvas();
		canvas.setPreferredSize(new Dimension(400, 400));
		canvas.setBackground(Color.WHITE);
		this.add(canvas, BorderLayout.CENTER);
		
		registerWindowEvents();
		registerMouseEvents();

		this.pack();
		this.setVisible(true);
	}
	
	private void registerWindowEvents() {

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				int answer = JOptionPane.showConfirmDialog(null, "Drawing will not be saved!\nReally close program?", "Close program?", JOptionPane.YES_NO_OPTION);
				if (answer == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
			}
		});
	}
	
	private void registerMouseEvents() {
		canvas.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent evt) {
				int xPos = evt.getX();
				int yPos = evt.getY();
				Graphics g = canvas.getGraphics();
				g.setColor(Color.BLACK);
				g.drawLine(xPos, yPos, xPos, yPos);
			}
		});

		canvas.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				int xPos = evt.getX();
				int yPos = evt.getY();
				Graphics g = canvas.getGraphics();
				g.setColor(Color.GREEN);
				g.drawOval(xPos, yPos, 5, 5);
			}
			
			public void mouseReleased(MouseEvent evt) {
				int xPos = evt.getX();
				int yPos = evt.getY();
				Graphics g = canvas.getGraphics();
				g.setColor(Color.RED);
				g.drawOval(xPos, yPos, 5, 5);
			}
		});

	}
}
