package ch.fhnw.richards.swing;

public class ButtonClickMVC {

	private static ButtonClickMVC mainProgram;
	
	private ButtonClickView view;
	private ButtonClickController controller;
	private ButtonClickModel model;
	
	public static void main(String[] args) {
		// Singleton pattern
		if (mainProgram == null) mainProgram = new ButtonClickMVC();
	}
	
	private ButtonClickMVC() {
		model = new ButtonClickModel();
		view = new ButtonClickView(model);
		controller = new ButtonClickController(model, view);
	}
	
	protected static ButtonClickMVC getMainProgram() {
		return mainProgram;
	}

	protected void endMainProgram() {
		view.setVisible(false);
		System.exit(0);
	}
}
