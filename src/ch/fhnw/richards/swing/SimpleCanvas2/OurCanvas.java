package ch.fhnw.richards.swing.SimpleCanvas2;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Iterator;

public class OurCanvas extends Canvas {
	private ArrayList<Point> points = new ArrayList<Point>();
	private ArrayList<Circle> circles = new ArrayList<Circle>();

	public OurCanvas() {
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(400, 400));

		this.addMouseMotionListener(new MouseAdapter() {
			public void mouseDragged(MouseEvent evt) {
				int xPos = evt.getX();
				int yPos = evt.getY();
				Graphics g = getGraphics(); // cannot say "this" because we are in an anon-class!
				g.setColor(Color.BLACK);
				g.drawLine(xPos, yPos, xPos, yPos);
				points.add(new Point(xPos, yPos));
			}
		});

		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent evt) {
				int xPos = evt.getX();
				int yPos = evt.getY();
				Graphics g = getGraphics();
				g.setColor(Color.GREEN);
				g.drawOval(xPos, yPos, 5, 5);
				circles.add(new Circle(xPos, yPos, 5, 5, Color.GREEN));
			}

			public void mouseReleased(MouseEvent evt) {
				int xPos = evt.getX();
				int yPos = evt.getY();
				Graphics g = getGraphics();
				g.setColor(Color.RED);
				g.drawOval(xPos, yPos, 5, 5);
				circles.add(new Circle(xPos, yPos, 5, 5, Color.RED));
			}
		});
	}

	public void paint(Graphics g) {
		// Loop using an iterator
		for (Iterator<Point> i = points.iterator(); i.hasNext();) {
			Point p = i.next();
			g.setColor(Color.BLACK);
			g.drawLine(p.x, p.y, p.x, p.y);
		}
		// Even easier
		for (Circle c : circles) {
			g.setColor(c.color);
			g.drawOval(c.x, c.y, c.width, c.height);
		}
	}
}
