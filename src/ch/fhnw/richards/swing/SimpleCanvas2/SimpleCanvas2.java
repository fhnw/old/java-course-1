package ch.fhnw.richards.swing.SimpleCanvas2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class SimpleCanvas2 extends JFrame {
	private OurCanvas canvas;

	public static void main(String[] args) {
		new SimpleCanvas2();
	}

	public SimpleCanvas2() {
		super("Simple Canvas 2");
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		this.setLayout(new BorderLayout());
		canvas = new OurCanvas();
		this.add(canvas, BorderLayout.CENTER);

		this.pack();
		this.setVisible(true);
	}
}
