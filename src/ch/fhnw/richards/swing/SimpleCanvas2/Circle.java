package ch.fhnw.richards.swing.SimpleCanvas2;

import java.awt.Color;

// Very simple class to store end-points of our lines
// Final, because it is not designed to be general, and
// should not be used elsewhere
final class Circle {
	public int x, y;
	public int width, height;
	public Color color;

	public Circle(int x, int y, int width, int height, Color color) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
	}
}
