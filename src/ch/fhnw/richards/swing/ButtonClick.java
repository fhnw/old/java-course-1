package ch.fhnw.richards.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ButtonClick extends JFrame implements ActionListener {
	private JLabel lblNumber;
	private JButton btnClick;

	public static void main(String args[]) {
		new ButtonClick();
	}

	// Create new ButtonClick
	public ButtonClick() {
		super("Button Click");
		getContentPane().setLayout(new GridLayout(2, 1));

		lblNumber = new JLabel();
		lblNumber.setText("0");
		getContentPane().add(lblNumber);

		btnClick = new JButton();
		btnClick.setText("Click Me!");
		getContentPane().add(btnClick);

		// The easy way to end the application
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		pack();
		btnClick.addActionListener(this);
		setVisible(true);
	}

	// Method for "ActionListener"
	public void actionPerformed(ActionEvent event) {
		int newValue;

		newValue = Integer.parseInt(lblNumber.getText()) + 1;
		lblNumber.setText(Integer.toString(newValue));
	}
}
