package ch.fhnw.richards.swing;

import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class ButtonClickView extends JFrame {
	protected JLabel lblNumber;
	protected JButton btnClick;
	
	// Links to other classes
	private ButtonClickModel model;

	protected ButtonClickView(ButtonClickModel model) {
		super("Button Click MVC");		
		this.model = model;
		
    this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE); // Disable standard handling
    this.setMinimumSize(new Dimension(400,200));
		getContentPane().setLayout(new GridLayout(2, 1));

		lblNumber = new JLabel();
		lblNumber.setHorizontalAlignment(SwingConstants.CENTER);
		lblNumber.setText(Integer.toString(model.getValue()));
		getContentPane().add(lblNumber);

		btnClick = new JButton();
		btnClick.setText("Click Me!");
		getContentPane().add(btnClick);

		pack();
		setVisible(true);
	}
}
