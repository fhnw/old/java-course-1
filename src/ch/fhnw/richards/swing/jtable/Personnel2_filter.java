package ch.fhnw.richards.swing.jtable;

import javax.swing.DefaultCellEditor;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;
import javax.swing.table.TableRowSorter;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class Personnel2_filter extends JFrame {

	public static void main(String[] args) {
		new Personnel2_filter();
	}

	public Personnel2_filter() {
		super();
		Container content = this.getContentPane();
		content.setLayout(new BorderLayout());

		PersonnelTableModel2 model = new PersonnelTableModel2();
		final JTable table = new JTable(model);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);
		TableRowSorter<PersonnelTableModel2> sorter = new TableRowSorter<PersonnelTableModel2>(model);
		table.setRowSorter(sorter);

		// Create the scroll pane and add the table to it.
		final JScrollPane scrollPane = new JScrollPane(table);
		content.add(scrollPane, BorderLayout.CENTER);

		// Create a table for filters
		final JTable filters = new JTable(new FilterModel(sorter));
		filters.setPreferredSize(new Dimension(500, filters.getRowHeight()));
		filters.setBackground(Color.PINK);
		content.add(filters, BorderLayout.SOUTH);
		DefaultCellEditor dce = (DefaultCellEditor) filters.getDefaultEditor(String.class);
		dce.setClickCountToStart(1);

		this.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent evt) {
				SwingUtilities.invokeLater(new Runnable() {
					public void run() {
						resizeFilters(scrollPane, table, filters);
					}
				});
			}
		});

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();

		// This is the proper, paranoid way to do it: after pack(),
		// all GUI modifications run in the event-dispatching thread,
		// even a simple setVisible();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setVisible(true);
			}
		});
	}

	// Make the filter-column size equal to those of the main table
	private void resizeFilters(JScrollPane scrollPane, JTable table, JTable filters) {
		for (int i = 0; i < table.getColumnCount(); i++) {
			 TableColumn tableColumn = table.getColumnModel().getColumn(i);
			 TableColumn filterColumn = filters.getColumnModel().getColumn(i);
			 filterColumn.setPreferredWidth(tableColumn.getWidth()); 
		}
		int width = 0;
		if (scrollPane.getVerticalScrollBar().isShowing()) {
			width = scrollPane.getVerticalScrollBar().getWidth();
		}
		filters.getColumnModel().getColumn(filters.getColumnCount()-1).setPreferredWidth(width);
		filters.getColumnModel().getColumn(filters.getColumnCount()-1).setMaxWidth(width);
		filters.getColumnModel().getColumn(filters.getColumnCount()-1).setMinWidth(width);
	}
}