package ch.fhnw.richards.swing.jtable;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

public class Personnel extends JFrame {
	private String[] columnNames = { "First Name", "Last Name", "Sport", "# of Years", "Vegetarian" };
	private Object[][] data = {
			{ "Mary", "Campione", "Snowboarding", new Integer(5), new Boolean(false) },
			{ "Alison", "Huml", "Rowing", new Integer(3), new Boolean(true) },
			{ "Kathy", "Walrath", "Knitting", new Integer(2), new Boolean(false) },
			{ "Sharon", "Zakhour", "Speed reading", new Integer(20), new Boolean(true) },
			{ "Philip", "Milne", "Pool", new Integer(10), new Boolean(false) } };

	public static void main(String[] args) {
		new Personnel();
	}

	public Personnel() {
		super();
		Container content = this.getContentPane();
		content.setLayout(new BorderLayout());

		JTable table = new JTable(data, columnNames);
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);

		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel.
		content.add(scrollPane, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();

		// This is the proper, paranoid way to do it: after pack(),
		// all GUI modifications run in the event-dispatching thread,
		// even a simple setVisible();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setVisible(true);
			}
		});
	}

}