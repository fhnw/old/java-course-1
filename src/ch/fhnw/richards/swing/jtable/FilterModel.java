package ch.fhnw.richards.swing.jtable;

import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

public class FilterModel extends AbstractTableModel {
	private Object[][] data = { { "", "", "", "", "", "" } };
	private TableRowSorter<PersonnelTableModel2> sorter;
	
	public FilterModel(TableRowSorter<PersonnelTableModel2> sorter) {
		super();
		this.sorter = sorter;
	}

	public int getColumnCount() {
		return data[0].length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return "";
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	// Only implement this method if your table is editable
	public boolean isCellEditable(int row, int col) {
		if (col < 3) {
			return true;
		} else {
			return false;
		}
	}
	
	// Only implement if the data in the table can change
	public void setValueAt(Object value, int row, int col) {
		data[row][col] = value;
		fireTableCellUpdated(row, col);
		setTableFilter((String) value, col);
	}

	private void setTableFilter(String filterText, int col) {
		RowFilter<PersonnelTableModel2, Object> rf = null;
	  //If current expression doesn't parse, don't update.
	  try {
	      rf = RowFilter .regexFilter(filterText, col);
	  } catch (java.util.regex.PatternSyntaxException e) {
	      return;
	  }
	  sorter.setRowFilter(rf);
	}

}



