package ch.fhnw.richards.swing.jtable;

import javax.swing.table.AbstractTableModel;

public class PersonnelTableModel2 extends AbstractTableModel {
	private String[] columnNames = { "First Name", "Last Name", "Sport", "# of Years", "Vegetarian" };
	private Object[][] data = {
			{ "Mary", "Campione", "Snowboarding", new Integer(5), new Boolean(false) },
			{ "Alison", "Huml", "Rowing", new Integer(3), new Boolean(true) },
			{ "Kathy", "Walrath", "Knitting", new Integer(2), new Boolean(false) },
			{ "Sharon", "Zakhour", "Speed reading", new Integer(20), new Boolean(true) },
			{ "Philip", "Milne", "Pool", new Integer(10), new Boolean(false) }, };

	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}

	public String getColumnName(int col) {
		return columnNames[col];
	}

	public Object getValueAt(int row, int col) {
		return data[row][col];
	}

	public Class getColumnClass(int c) {
		return getValueAt(0, c).getClass();
	}

	// Only implement this method if your table is editable
	public boolean isCellEditable(int row, int col) {
		if (col < 2) {
			return false;
		} else {
			return true;
		}
	}

	// Only implement if the data in the table can change
	public void setValueAt(Object value, int row, int col) {
		data[row][col] = value;
		fireTableCellUpdated(row, col);
	}
}
