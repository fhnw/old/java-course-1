package ch.fhnw.richards.swing.jtable;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;

public class Personnel3  extends JFrame {
		private JTextField txtFirstName;
		private JTextField txtLastName;
		private JTextField txtSport;
		private JTextField txtYears;
		private JTextField txtVeggie;
		
		public static void main(String[] args) {
			new Personnel3();
		}

		public Personnel3() {
			super();
			Container content = this.getContentPane();
			content.setLayout(new BorderLayout());

			final PersonnelTableModel3 model = new PersonnelTableModel3();
			final JTable table = new JTable(model);
			table.setPreferredScrollableViewportSize(new Dimension(500, 70));
			table.setFillsViewportHeight(true);
			table.setAutoCreateRowSorter(true);
			table.getSelectionModel().addListSelectionListener(
						new ListSelectionListener() {
							@Override
							public void valueChanged(ListSelectionEvent arg0) {
								int viewRow = table.getSelectedRow();
								if (viewRow >= 0) { // < 0 if selection filtered out
									int modelRow = table.convertRowIndexToModel(viewRow);
									txtFirstName.setText(model.getValueAt(modelRow, 0).toString());
									txtLastName.setText(model.getValueAt(modelRow, 1).toString());
									txtSport.setText(model.getValueAt(modelRow, 2).toString());
									txtYears.setText(model.getValueAt(modelRow, 3).toString());
									txtVeggie.setText(model.getValueAt(modelRow, 4).toString());
								}
							}
						}
					);
			
			// Create the scroll pane and add the table to it.
			JScrollPane scrollPane = new JScrollPane(table);

			// Add the scroll pane to this panel.
			content.add(scrollPane, BorderLayout.CENTER);
			
			// Set up the detail form
			JPanel detailPanel = new JPanel();
			detailPanel.setLayout(new GridLayout(3,4,10,10));
			content.add(detailPanel, BorderLayout.SOUTH);
			
			JLabel lbl = new JLabel("First name"); detailPanel.add(lbl);
			txtFirstName = new JTextField(); detailPanel.add(txtFirstName);
			lbl = new JLabel("Last name"); detailPanel.add(lbl);
			txtLastName = new JTextField(); detailPanel.add(txtLastName);
			lbl = new JLabel("Sport"); detailPanel.add(lbl);
			txtSport = new JTextField(); detailPanel.add(txtSport);
			lbl = new JLabel("# of years"); detailPanel.add(lbl);
			txtYears = new JTextField(); detailPanel.add(txtYears);
			lbl = new JLabel("Vegetarian"); detailPanel.add(lbl);
			txtVeggie = new JTextField(); detailPanel.add(txtVeggie);

			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pack();

			// This is the proper, paranoid way to do it: after pack(),
			// all GUI modifications run in the event-dispatching thread,
			// even a simple setVisible();
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					setVisible(true);
				}
			});
		}
	}