package ch.fhnw.richards.swing.jtable;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;

public class Personnel2 extends JFrame {
	public static void main(String[] args) {
		new Personnel2();
	}

	public Personnel2() {
		super();
		Container content = this.getContentPane();
		content.setLayout(new BorderLayout());

		JTable table = new JTable(new PersonnelTableModel2());
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
		table.setFillsViewportHeight(true);

		// Create the scroll pane and add the table to it.
		JScrollPane scrollPane = new JScrollPane(table);

		// Add the scroll pane to this panel.
		content.add(scrollPane, BorderLayout.CENTER);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();

		// This is the proper, paranoid way to do it: after pack(),
		// all GUI modifications run in the event-dispatching thread,
		// even a simple setVisible();
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				setVisible(true);
			}
		});
	}

}