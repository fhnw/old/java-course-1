package ch.fhnw.richards.swing.jtable;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * A simple demo of JTable usage: display a table that shows the Sieve of Erastothenes
 * as it performs the first few steps...
 * 
 * @author brad
 *
 */
public class SieveOfErastothenes extends JFrame implements Runnable {
  private Integer[] numbers;

  public static void main(String[] args) {
    SieveOfErastothenes s = new SieveOfErastothenes(); // set up GUI
    Thread t = new Thread(s, "Calculations"); // Thread for calculations
    t.start();
  }

  private SieveOfErastothenes() {
    String[] columnNames = {"2","3","4","5","6","7","8","9","10","11",
"12","13","14","15","16","17","18","19","20","21",
"22","23","24","25","26","27","28","29","30","31"};
    Integer[][] tableData = {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
    numbers = tableData[0];
    JTable numbersTable = new JTable(tableData, columnNames);
    this.setPreferredSize(new Dimension(700,80));
    this.setLayout(new BorderLayout());
    this.add(numbersTable.getTableHeader(), BorderLayout.NORTH);
    this.add(numbersTable, BorderLayout.CENTER);
    this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    this.pack();
    this.setVisible(true);
  }
  
  @Override
  public void run() {
    // Check each number
    for (int i = 0; i < numbers.length; i++) {
      if (numbers[i] == 0) { // found a prime
        int prime = i+2;
        // Mark all multiples of the prime
        for (int j = i+prime; j < numbers.length; j+=prime) {
          numbers[j] = 1;
          try {
            Thread.sleep(400);
          } catch (Exception e) {
          }
          this.repaint(); // update display to reflect changes
        }
      }
    }
  }
}