package ch.fhnw.richards.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ButtonClickController implements ActionListener {
	
	private ButtonClickModel model;
	private ButtonClickView view;
	
	protected ButtonClickController(ButtonClickModel model, ButtonClickView view) {
		this.model = model;
		this.view = view;
		
		// register ourselves to listen for button clicks
		view.btnClick.addActionListener(this);

		// register ourselves to handle window-closing event
		view.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				ButtonClickMVC.getMainProgram().endMainProgram();
			}
		});	
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		model.incrementValue();
		String newText = Integer.toString(model.getValue());
		view.lblNumber.setText(newText);		
	}	
}
