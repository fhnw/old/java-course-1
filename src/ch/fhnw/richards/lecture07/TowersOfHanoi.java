package ch.fhnw.richards.lecture07;

/**
 * Solving the "Towers of Hanoi" problem using arrays.
 * 
 * @author brad
 *
 */
public class TowersOfHanoi {
	private static final int NUMBER_OF_DISKS = 3;
	private static int[] A = new int[NUMBER_OF_DISKS];
	private static int[] B = new int[NUMBER_OF_DISKS];
	private static int[] C = new int[NUMBER_OF_DISKS];

	public static void main(String[] args) {
		// Set up the problem
		for (int i = 0; i < NUMBER_OF_DISKS; i++) {
			A[i] = NUMBER_OF_DISKS - i;
			B[i] = 0; // in fact, already zero
			C[i] = 0; // in fact, already zero
		}
		
		// Solve the problem: move disks from A to C beginning with disk A[0],
		// and using B as intermediate storage.
		hanoi(A, C, B, NUMBER_OF_DISKS);
	}
	
	/**
	 * Move the given number of disks from the "from" post to the "to" post, using
	 * the "storage" post for intermediate storage
	 * 
	 * @param from The post currently holding the disks
	 * @param to The post where the disks should be placed
	 * @param storage The third post, where we can temporarily place disks
	 * @param disksToMove The number of disks to move
	 */
	private static void hanoi(int[] from, int[] to, int[] storage, int disksToMove) {
		if (disksToMove == 1) { // base case
			int disk = takeDisk(from);
			placeDisk(to, disk);
			printGameStatus();
		} else {
			hanoi(from, storage, to, disksToMove-1);
			int disk = takeDisk(from);
			placeDisk(to, disk);
			printGameStatus();
			hanoi(storage, to, from, disksToMove-1);
		}
	}
	
	/**
	 * Take the top disk from the given post. This method removes the disk from the
	 * post, and returns it as an integer.
	 * 
	 * One error is possible: If the post contains no disks this method will return 0
	 * 
	 * @param post The post containing zero or more disks
	 * @return An integer representing the disk removed from the post
	 */
	private static int takeDisk(int[] post) {
		int disk = 0;
		int diskPos = NUMBER_OF_DISKS;
		
		while (disk == 0 & diskPos > 0) {
			diskPos--;
			disk = post[diskPos];
			post[diskPos] = 0;
		}
		return disk;
	}
	
	/**
	 * Place a disk onto a post. This method has no return value. Two errors can
	 * occur: (1) the disk to be placed is larger than the disk it is placed on,
	 * and (2) the post is already full. In case of an error, this method does
	 * nothing - the post remains unchanged.
	 * 
	 * @param post The post to receive the disk
	 * @param disk The disk to be placed onto the post
	 */
	private static void placeDisk(int[] post, int disk) {
		boolean error = false;
		
		int diskPos = 0;
		while (post[diskPos] > 0 & diskPos < NUMBER_OF_DISKS) {
			diskPos++;
		}
		
		// Is the post full
		if (diskPos >= NUMBER_OF_DISKS) error = true;
		
		// Is the disk beneath the new one larger?
		if (diskPos > 0 && disk >= post[diskPos-1]) error = true;
		
		// If no error, place the disk
		if (!error) {
			post[diskPos] = disk;
		}
	}
	
	/**
	 * Print the current status of the game - display the contents of the three
	 * posts (A, B, and C) each on a line in the console. Do not display empty
	 * places on the posts (i.e., zeroes).
	 */
	private static void printGameStatus() {
		for (int disk : A) {
			if (disk > 0) System.out.print(disk + " ");
		}
		System.out.println();
		
		for (int disk : B) {
			if (disk > 0) System.out.print(disk + " ");
		}
		System.out.println();
		
		for (int disk : C) {
			if (disk > 0) System.out.print(disk + " ");
		}
		System.out.println();
		System.out.println();
	}
}
