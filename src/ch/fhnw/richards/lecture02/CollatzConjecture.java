package ch.fhnw.richards.lecture02;

import java.util.Scanner;

public class CollatzConjecture {

	public static void main(String[] args) {
		// Define the variables we need
		int testNumber;
		int count;
		
		// Read a testNumber from the console
		System.out.print("Number to test: ");
		Scanner s = new Scanner(System.in);
		testNumber = s.nextInt();
		
		// Initialize before the loop
		count = 0;
		
		// Loop until the number becomes 1
		while (testNumber != 1) {
			// if even then n/2; if odd then 3n+1
			if (testNumber % 2 == 0) {
				testNumber = testNumber / 2;
			} else {
				testNumber = 3 * testNumber + 1;
			}
			
			// increment count
			count++;
		}
		
		// Post-processing after the loop
		System.out.println("Number of iterations required: " + count);
	}
}
