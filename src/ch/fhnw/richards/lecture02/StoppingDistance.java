package ch.fhnw.richards.lecture02;

/**
 * A program to calculate the stopping distance of cars. The stopping distance is the sum of
 * reaction-distance and braking-distance.
 * 
 * @author brad
 *
 */
public class StoppingDistance {

	public static void main(String[] args) {
		// All variables are floats - why is this the correct choice?
		float velocity = 33.333f; // 120 km/h
		float reactionTime = 0.3f; // in seconds
		
		// Calculate reaction distance
		float reactionDistance = reactionTime * velocity;
		System.out.println("Reaction distance: " + reactionDistance);
		
		// Calculate braking distance
		float brakingDistance = velocity * velocity / 16.0f;
		System.out.println("Braking distance: " + brakingDistance);
		
		// Calculate the stopping distance
		float stoppingDistance = reactionDistance + brakingDistance;
		System.out.println("Stopping distance: " + stoppingDistance);
	}
}
