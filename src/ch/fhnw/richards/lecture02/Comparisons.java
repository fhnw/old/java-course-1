package ch.fhnw.richards.lecture02;

public class Comparisons {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String x = "";
		String y = "abc";
		String z = "abc";
		x += "abc";

		String s = "Strings x==y: ";
		if (x == y)	s += "true"; else	s += "false";
		System.out.println(s);

		s = "Strings y==z: ";
		if (y == z)	s += "true"; else	s += "false";
		System.out.println(s);

		for (int i = 15; i < 1000; i = i * 2) {
			Integer a = i;
			Integer b = i;
			s = "Value " + i + ":";
			if (a == b)	s += " a == b "; else	s += "a != b";
			System.out.println(s);
		}
	}
}
