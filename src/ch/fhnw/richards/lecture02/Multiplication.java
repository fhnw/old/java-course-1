package ch.fhnw.richards.lecture02;

/**
 * Solution to the exercise: print a multiplication table.
 * 
 * For printing, we use "printf", which takes a format string.
 * We do this to ensure that we always print 3 characters, so
 * everything lines up nicely. %3d means a decimal integer
 * occupying 3 characters.
 */
public class Multiplication {

	public static void main(String[] args) {
		// Print the first line
		System.out.print("   ");
		for (int i = 1; i < 10; i++) {
			System.out.printf("%3d", i);
		}
		System.out.println();
		
		// Print the multiplication table
		for (int i = 1; i < 10; i++) {
			// Print the left column
			System.out.printf("%3d", i);
			for (int j = 1; j < 10; j++) {
				int answer = i * j;
				// Print one entry in the table
				System.out.printf("%3d", answer);
			}
			System.out.println();
		}
	}
}
