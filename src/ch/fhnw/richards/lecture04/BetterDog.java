package ch.fhnw.richards.lecture04;

public class BetterDog {
  private String name;
  private String breed; // should be a class itself
  private int height; // centimeter at the shoulder
  
  /**
   * Create a new dog. Items that determine the dog's
   * identity are set in the constructor and cannot be
   * changed.
   * 
   * @param name - the name of the dog
   * @param breed - the breed of the dog
   */
  public BetterDog(String name, String breed) {
  	this.name = name;
  	this.breed = breed;
  }
  
  /**
   * Create a new dog. This constructor also includes
   * additional items, in case these are known when the
   * dog is created.
   * 
   * @param name - the name of the dog
   * @param breed - the breed of the dog
   * @param height - height at the shoulder in centimeters
   */
  public BetterDog(String name, String breed, int height) {
  	this.name = name;
  	this.breed = breed;
  	this.height = height;
  }

  // Getter and setter methods
  public String getName() {
  	return name;
  }
  public String getBreed() {
  	return breed;
  }
  public void setHeight(int height) {
  	this.height = height;
  }
  public int getHeight() {
  	return height;
  }
  
  /**
   * @param none
   * @return (float) The dog's estimated weight in kg
   */
  public float weight() {
    return (height * height * height / 3000);
  }
  
  public boolean equals(Dog d) {
	  boolean areEqual = false;
	  if (d != null) {
		  areEqual = (d.name == this.name && d.breed == this.breed);
	  }
	  return areEqual;
  }
}
