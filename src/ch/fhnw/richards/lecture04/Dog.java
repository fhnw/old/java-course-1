package ch.fhnw.richards.lecture04;

public class Dog {
  public String name;
  public String breed; // should be a class itself
  public int height; // centimeter at the shoulder
  /**
   * @param none
   * @return (float) The dog's estimated weight in kg
   */
  public float weight() {
    return (height * height * height / 3000);
  }
}
