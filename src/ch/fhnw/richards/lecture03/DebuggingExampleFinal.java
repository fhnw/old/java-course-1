package ch.fhnw.richards.lecture03;

public class DebuggingExampleFinal {

	public static void main(String[] args) {
		int[] numsIn;
		int[] numsOut;
		
		// Convert command-line args to integer array
		numsIn = new int[args.length];
		for (int i = 0; i < args.length; i++) {
			numsIn[i] = Integer.parseInt(args[i]);
		}
		
		// Create the second array
		int lengthNumsOut = (numsIn.length + 1) / 2;
		numsOut = new int[lengthNumsOut];		
		
		// Process the first array into the second
		for (int i = 0; i < lengthNumsOut; i++) {
			numsOut[i] = numsIn[i] + numsIn[numsIn.length - i - 1];
		}
		
		// Print the result
		for (int num : numsOut) {
			System.out.print(num + ", ");
		}
	}

}
