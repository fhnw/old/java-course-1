package ch.fhnw.richards.lecture03;

public class StringReverser {

	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.out.println("Exactly one argument required");
			System.exit(0);
		}
		
		char[] chars = args[0].toCharArray();
		// Watch out for off-by-one errors!
		int stringMiddle = ( chars.length / 2);
		for (int i = 0; i < stringMiddle; i++) {
			char tmpChar = chars[i];
			chars[i] = chars[chars.length - i - 1];
			chars[chars.length - i - 1] = tmpChar;
		}
		String reversed = new String(chars);
		System.out.println(reversed);
	}
}
