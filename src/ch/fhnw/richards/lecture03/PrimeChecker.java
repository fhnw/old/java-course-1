package ch.fhnw.richards.lecture03;

public class PrimeChecker {

	public static void main(String[] args) {
		boolean argOK;
		int numberToCheck = 0;
		
		// Fetch the command-line argument
		argOK = false;
		if (args.length != 1) {
			System.out.println("Exactly one argument, please.");
		} else {
			numberToCheck = Integer.parseInt(args[0]);
			if ( numberToCheck < 2 ) {
				System.out.println("Must be at least 2");
			} else {
				argOK = true;
			}
		}		
		
		if (argOK) {
			// The simplest possible way to check
			boolean isPrime = true;
			int maxDivisor = (int) java.lang.Math.sqrt(numberToCheck);
			for (int divisor = 2; divisor <= maxDivisor; divisor++) {
				if ( (numberToCheck % divisor) == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime) {
				System.out.println("yes, " + numberToCheck + " is prime");
			} else {
				System.out.println("no, " + numberToCheck + " is not prime");
			}
		}
	}
}
