package ch.fhnw.richards.lecture03;

public class Dice {
	public static void main(String args[]) {
		final int REPEATS = 1000;
		int numberOfDice = 0;
		boolean argOK;
		
		// Fetch the command-line argument
		argOK = false;
		if (args.length != 1) {
			System.out.println("Exactly one argument, please.");
		} else {
			numberOfDice = Integer.parseInt(args[0]);
			if ( (numberOfDice < 0) | (numberOfDice > 10) ) {
				System.out.println("Must be between 1 and 10");
			} else {
				argOK = true;
			}
		}		
		
		if (argOK) {
			// Initialize variables
			double idealAverage = numberOfDice * 3.5;
			double totalAverage;
			int totalAllRolls = 0;
			double rollVariance;
			double totalVariance = 0;
			
			// Loop 1000 times
			for (int i = 0; i < REPEATS; i++) {
				// Initialize sum to zero
				int rollSum = 0;
				
				// Loop n times, where n is the number of dice
				for (int die = 0; die < numberOfDice; die++) {
					rollSum += rollDie();					
				}
				rollVariance = java.lang.Math.abs(rollSum - idealAverage);
				rollVariance *= rollVariance;
				totalVariance += rollVariance;
				totalAllRolls += rollSum;
			}			
			// Calculate average and standard deviation
			totalAverage = ((float) totalAllRolls) / REPEATS;
			totalVariance = totalVariance / REPEATS;
			double stdDeviation = java.lang.Math.sqrt(totalVariance);
			
			System.out.println("Average is " + totalAverage);
			System.out.println("Variance from ideal " + totalVariance);
			System.out.println("Standard deviation " + stdDeviation);
		}
	}
	
	private static int rollDie() {
		double rand;
		int randInt;
		rand = java.lang.Math.random();
		rand *= 6;
		randInt = (int) rand;
		randInt += 1;
		return randInt;
	}
}
