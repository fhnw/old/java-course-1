package ch.fhnw.richards.familyRelationships.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class Person {
	public static enum Gender { MALE, FEMALE };
	
	private String name; // Names are assumed to be unique and unchangeable
	private Gender gender;
	private boolean alive; // TODO
	private Date birthDate; // TODO
	private Date deathDate; // TODO
	private Person father;
	private Person mother;
	private ArrayList<Person> children;
	
	/**
	 * Utility function: Given an ArrayList<Person> and a name, find and return the named person
	 */
	public static Person findPerson(ArrayList<Person> people, String name) {
		Person foundPerson = null;
		for (Iterator<Person> i = people.iterator(); i.hasNext() && foundPerson == null; ) {
			Person p = i.next();
			if (name.equals(p.getName())) {
				foundPerson = p;
			}
		}
		return foundPerson;
	}

	/**
	 * Constructor
	 * 
	 * @param name
	 *            The name of the person
	 */
	public Person(String name) {
		this.name = name;
		children = new ArrayList<Person>();
	}

	/**
	 * Every class should override equals - First, the object is not null -
	 * Then, check the class of the object - Finally, we compare the names
	 */
	@Override
	public boolean equals(Object objIn) {
		boolean objectsEqual = false;
		if (objIn != null) {
			if (objIn.getClass() == this.getClass()) {
				Person p = (Person) objIn;
				objectsEqual = name.equals(p.getName());
			}
		}
		return objectsEqual;
	}

	public String getName() {
		return name;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Gender getGender() {
		return gender;
	}

	// We only set the father if not already set
	// Check: Is the person male?
	// Check: If father is already set to
	// something different, what do we do?
	public void setFather(Person father) {
		if (this.father == null) this.father = father;
		father.addChild(this);
	}

	public Person getFather() {
		return father;
	}

	// We only set the mother if not already set
	// Check: Is the person male?
	// Check: If mother is already set to
	// something different, what do we do?
	public void setMother(Person mother) {
		if (this.mother == null) this.mother = mother;
		mother.addChild(this);
	}

	public Person getMother() {
		return mother;
	}

	// Only add child if not already present
	public void addChild(Person child) {
		boolean found = false;
		for (Person c : children) {
			if (c.equals(child)) found = true;
		}

		if (!found) {
			children.add(child);
			if (this.getGender() == Gender.MALE) {
				child.setFather(this);
			} else {
				child.setMother(this);
			}
		}
	}

	public ArrayList<Person> getChildren() {
		return children;
	}

	// ------------------------------
	// Methods implementing commands
	// ------------------------------

	/**
	 * Return a list containing the grandparents for this person.
	 * 
	 * Check: what about null values?
	 */
	public ArrayList<Person> getGrandparents() {
		ArrayList<Person> grandparents = new ArrayList<Person>();

		Person mother = this.getMother();
		Person father = this.getFather();

		grandparents.add(mother.getFather());
		grandparents.add(mother.getMother());
		grandparents.add(father.getFather());
		grandparents.add(father.getMother());

		return grandparents;
	}
	
	// TODO: More methods for more commands
}
