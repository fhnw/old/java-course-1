package ch.fhnw.richards.familyRelationships;

import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;

import ch.fhnw.richards.familyRelationships.model.Person;

/**
 * This is the main program. It performs the following tasks
 * 
 * - Set up the program, including shared resources, using static attributes
 * -- Logger: the standard logger to use for this program
 * -- Family: the ArrayList of all people in the family
 * 
 * - Read in any existing family members from a file
 * 
 * - Open the Person window
 *  
 * @author brad
 *
 */
public class FamilyRelationships extends JFrame {
	// Resources are static singletons (method 2 from the lecture)
	private static FamilyRelationships mainProgram;
	private static Logger logger;
	private static ArrayList<Person> family;
	
	private static final String LOGGER_NAME = FamilyRelationships.class.getSimpleName();
	private static final Level CONSOLE_LEVEL = Level.INFO; // The level of messages to display on the console 

	public static void main(String[] args) {
		// Only one instance of this program may be running (per JVM)
		if (mainProgram == null) {
			// set up logging first, so even the main constructor can use it
			logger = Logger.getLogger(LOGGER_NAME);
			Handler consoleHandler = Logger.getLogger("").getHandlers()[0];
			consoleHandler.setLevel(CONSOLE_LEVEL);
			
			// set up the family, reading in any persons saved previously
			family = new ArrayList<Person>();
			loadFamilyFile();
			
			// create the main program instance (singleton)
			mainProgram = new FamilyRelationships();
		}		
	}

	/**
	 * Getter for shared resource "family"
	 */
	public static ArrayList<Person> getFamily() {
		return family;
	}

	/**
	 * Getter for shared resource "logger"
	 */
	public static Logger getLogger() {
		return logger;
	}

	/**
	 * Our main program window allows us to enter and delete people, and also to display the reports window
	 */
	private FamilyRelationships() {
		// TODO
	}
	
	/**
	 * Load people from an existing family-file
	 */
	private static void loadFamilyFile() {
		// TODO
	}
	
	/**
	 * Save all people into a family file
	 */
	private static void saveFamilyFile() {
		// TODO
	}
}
