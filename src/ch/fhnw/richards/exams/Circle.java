package ch.fhnw.richards.exams;

public class Circle extends Shape {
  public static final double PI = 3.141592653589793238;
  private double radius;
  public Circle(double radius) {
  	this.radius = radius;
  }
  public double getRadius() {
  	return radius;
  }
  public double area() {
  	return PI * radius * radius;
  }
  public double circumference() {
  	return PI * 3 * radius;
  }
}
