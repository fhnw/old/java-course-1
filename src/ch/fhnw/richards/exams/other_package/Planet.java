package ch.fhnw.richards.exams.other_package;

import ch.fhnw.richards.exams.astronomy.AstroBody;

public class Planet extends AstroBody {

	public Planet() {
		name = "My planet";
		diameter = 0;
//		mass = 0;
		orbiting = null;
	}

}
