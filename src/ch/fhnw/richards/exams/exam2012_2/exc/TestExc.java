package ch.fhnw.richards.exams.exam2012_2.exc;

import java.util.Scanner;

public class TestExc {
	
	public static class ScaryException extends Exception {
		public ScaryException(String msg) {
			super(msg);
		}
	}

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int x = s.nextInt();
		
		try {
			checkInput(x);
		} catch (ScaryException e) {
			System.out.println("I am scared");
		} catch (Exception e) {
			System.out.println(e.toString());
		} finally {
			System.out.println("Finished");
		}

	}
	
	public static void checkInput(int x) throws Exception {
		if (x == 13) {
			ScaryException my = new ScaryException("scary number");
			throw my;
		} else if (x < 0) {
			Exception e = new Exception("x cannot be negative");
			throw e;
		}
	}
}
