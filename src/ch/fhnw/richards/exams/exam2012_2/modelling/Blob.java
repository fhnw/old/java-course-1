package ch.fhnw.richards.exams.exam2012_2.modelling;

public class Blob {
	private Stick[] sticks;

	public Blob(int numberOfSticks) {
		sticks = new Stick[numberOfSticks];
	}
	
	/**
	 * Check whether this blob is attached to a specific stick
	 */
	public boolean isAttached(Stick stick) {
		boolean isAttached = false;
		for (Stick s : sticks) {
			if (s != null && s == stick) {
				isAttached = true;
			}
		}
		return isAttached;
	}
	
	/**
	 * Attach a stick to this blob; this method is called from the superclass.
	 */
	boolean attach(Stick newStick) {
		boolean success = false;
		
		// Find first empty space in array
		int empty = -1;
		for (int i = 0; i < sticks.length && empty == -1; i++) {
			if (sticks[i] == null) empty = i;
		}

		if (empty > -1) {
			sticks[empty] = newStick;
			success = true;
		}
		
		return success;
	}
	
	/**
	 * Detach a stick from this blob;
	 * this method is called from the superclass.
	 */
	boolean detach(Stick oldStick) {
		boolean success = false;
		
		int position = -1;
		if (oldStick != null) {
			// Find the stick in array
			for (int i = 0; i < sticks.length && position == -1; i++) {
				if (oldStick.equals(sticks[i])) position = i;
			}
		}
		
		if (position > -1) {
			sticks[position] = null;
			success = true;
		}
		
		return success;
	}

	//----- Getter Methods -----
	
	/**
	 * Return a *copy*, to prevent external classes from changing the array
	 */
	public Stick[] getSticks() {
		return sticks.clone();
	}	
}