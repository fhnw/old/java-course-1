package ch.fhnw.richards.exams.exam2012_2.modelling;

public class Builder {
	/**
	 * This method attaches a blob and a stick together.
	 * See the exam problem for a detailed specification.
	 */
	public static boolean attach(Blob blob, Stick stick) {
		boolean success = false;
		if (!blob.isAttached(stick)) {
			// attach blob-to-stick and stick-to-blob.
			// In case of failure, undo!
			if (blob.attach(stick)) {
				if (stick.attach(blob)) {
					success = true;
				} else {
					blob.detach(stick);
				}
			}
		}
		return success;
	}

	/**
	 * This method detaches a blob and a stick.
	 * See the exam problem for a detailed specification
	 */
	public static boolean dettach(Blob blob, Stick stick) {
		// We ignore errors! If the objects were not attached,
		// it doesn't matter. No matter what, at the end of
		// this method, the objects will be detached.
		blob.detach(stick);
		stick.detach(blob);
		return true;
	}	
}
