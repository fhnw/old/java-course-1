package ch.fhnw.richards.exams.exam2012_2.modelling;

public class Stick {
	private Blob end1 = null;
	private Blob end2 = null;

	/**
	 * Check whether this stick is attached to a specific blob
	 */
	public boolean isAttached(Blob blob) {
		boolean isAttached = false;
		if ((end1 != null && end1 == blob)
				| (end2 != null && end2 == blob)) {
			isAttached = true;
		}
		return isAttached;
	}

	/**
	 * Attach this stick to a blob; this method is called from the superclass.
	 */
	boolean attach(Blob newBlob) {
		boolean success = false;
		
		if (end1 == null) {
			end1 = newBlob;
			success = true;
		} else if (end2 == null) {
			end2 = newBlob;
			success = true;
		}
		return success;
	}
	
	/**
	 * Detach this stick from a blob; this method is called from the superclass.
	 */
	boolean detach(Blob oldBlob) {
		boolean success = false;
		
		if (oldBlob != null) {
			if (oldBlob == end1) {
				end1 = null;
				success = true;
			} else if (oldBlob == end2) {
				end2 = null;
				success = true;
			}
		}
		return success;
	}

	// ----- Getter Methods -----

	public Blob getEnd1() {
		return end1;
	}

	public Blob getEnd2() {
		return end2;
	}
}
