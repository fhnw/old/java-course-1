package ch.fhnw.richards.exams.exam2012_2.math;

/**
 * Die Hierarchie numerischer Datentypen in Java wird rechts gezeigt. Wir
 * möchten diese Hierarchie mit rationalen Zahlen (Bruchteilen, englisch
 * "Fractions") erweitern. Eine rationale Zahl besteht aus zwei Zahlen:
 * Zähler/Nenner (englisch: numerator/denominator).
 * 
 * Unsere Klasse "Fraction" wird:
 * 
 * - die bestehende abstrakte Klasse "Number" erweitern. Die astrakte Klasse
 * verlangt, dass wir die vier Methoden intValue(), longValue(), floatValue()
 * und doubleValue() implementieren.
 * 
 * - die Inferface "Comparable" implementieren. Eine Fraction muss mit allen
 * anderen Subklassen von "Number" vergleichbar sein.
 * 
 * - die Methode "equals()" überschreiben. Hier sollen numerische Werte
 * verglichen werden; eine Fraction muss mit allen anderen Subklassen von Number
 * vergleichbar sein. Achtung: Die korrekte Deklaration der "equals" Methode
 * nimmt einen Parameter vom Typ "Object".
 * 
 * Ergänzen Sie die fehlenden Teile der Implementation.
 * 
 */

public class TestFraction {

	public static void main(String[] args) {
		Fraction a = new Fraction(15, 45); // The value 1 / 3
		Fraction b = new Fraction(16, 64); // The value 1 / 4
		Fraction c = a.add(b);
		System.out.println(a + " + " + b + " = " + c);

		System.out.println("Comparing a and b: " + a.compareTo(b));
		System.out.println("Comparing a and b: " + a.equals(b));
	}

}
