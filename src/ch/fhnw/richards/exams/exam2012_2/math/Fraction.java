package ch.fhnw.richards.exams.exam2012_2.math;

public class Fraction extends Number implements Comparable<Number> {
	private long numerator;
	private long denominator;

	public Fraction(long numerator, long denominator) {
		this.numerator = numerator;
		this.denominator = denominator;
		this.reduce();
	}

	public Fraction add(Fraction f) {
		long numerator1 = this.numerator * f.denominator;
		long numerator2 = f.numerator * this.denominator;
		long tmpDenominator = this.denominator * f.denominator;
		long tmpNumerator = numerator1 + numerator2;
		return new Fraction(tmpNumerator, tmpDenominator);
	}
	
	public Fraction multiply(Fraction f) {
		long tmpNumerator = this.numerator * f.numerator;
		long tmpDenominator = this.denominator * f.denominator;
		return new Fraction(tmpNumerator, tmpDenominator);
	}

	private void reduce() {
		long gcd;
		if (numerator > denominator) {
			gcd = getGCD(numerator, denominator);
		} else {
			gcd = getGCD(denominator, numerator);
		}
		if (gcd > 1) {
			numerator = numerator / gcd;
			denominator = denominator / gcd;
		}
	}

	private long getGCD(long x, long y) {
		while (y != 0) {
			long z = x % y;
			x = y;
			y = z;
		}
		return x;
	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append(Long.toString(numerator));
		s.append('/');
		s.append(Long.toString(denominator));
		return s.toString();
	}

	@Override // required by the superclass Number
	public int intValue() {
		return (int) (numerator / denominator);
	}

	@Override // required by the superclass Number
	public long longValue() {
		return numerator / denominator;
	}

	@Override // required by the superclass Number
	public float floatValue() {
		return (float) numerator / (float) denominator;
	}

	@Override // required by the superclass Number
	public double doubleValue() {
		return (double) numerator / (double) denominator;
	}

	/**
	 * Im Fall, dass der Parameter null ist, darf die compareTo Methode eine
	 * Null-Pointer Exception generieren.
	 */
	@Override // required by the interface Comparable
	public int compareTo(Number o) {
		Double oValue = o.doubleValue();
		Double thisValue = this.doubleValue();
		return thisValue.compareTo(oValue);
	}
	
	/**
	 * Die equals() Methode soll auf keinen Fall eine Exception generieren.
	 */
	@Override // defined in java.lang.Object
	public boolean equals(Object o) {
		boolean isEqual = false;
		if (o != null) {
			if (o instanceof Number) {
				Number oNumber = (Number) o;
				isEqual = (this.compareTo(oNumber) == 0);
			}
		}
		return isEqual;
	}
}
