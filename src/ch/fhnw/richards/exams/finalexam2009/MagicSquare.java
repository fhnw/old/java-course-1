package ch.fhnw.richards.exams.finalexam2009;

public class MagicSquare {

	private int[][] square = { { 7, 12, 1, 14 }, { 2, 13, 8, 11 }, { 16, 3, 10, 5 }, { 9, 6, 15, 4 } };

	public static void main(String[] args) {
		new MagicSquare();
	}

	private MagicSquare() {
		System.out.println(isMagic(square));
	}

	private boolean isMagic(int[][] square) {
		int sum;

		// Check the size: is it non-empty and is it square?
		boolean squareOk = true;
		int size = square.length;
		if (size > 0) {
			if (square[0].length != size) {
				squareOk = false;
			}
		} else {
			squareOk = false;
		}

		if (squareOk) {

			// Calculate the magic number
			int magic = (size * size) * (size * size + 1) / (2 * size);

			// Check the rows
			for (int i = 0; i < size; i++) {
				sum = 0;
				for (int j = 0; j < size; j++) {
					int elt = square[i][j];
					sum += elt;
				}
				if (sum != magic) squareOk = false;
			}

			// Check the columns
			for (int i = 0; i < size; i++) {
				sum = 0;
				for (int j = 0; j < size; j++) {
					int elt = square[j][i];
					sum += elt;
				}
				if (sum != magic) squareOk = false;
			}

			// Check the diagonals
			sum = 0;
			for (int i = 0; i < size; i++) {
				int elt = square[i][i];
				sum += elt;
			}
			if (sum != magic) squareOk = false;

			sum = 0;
			for (int i = 0; i < size; i++) {
				int elt = square[size - i - 1][i];
				sum += elt;
			}
			if (sum != magic) squareOk = false;
		}

		return squareOk;
	}
}