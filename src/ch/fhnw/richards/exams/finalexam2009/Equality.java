package ch.fhnw.richards.exams.finalexam2009;

public class Equality {
	public static void main(String[] args) {
		Equality e = new Equality();
		e.runTests();
	}

	private void runTests() {
		Stuff s1 = new Stuff("woof", 13);
		Stuff s2 = new Stuff("woof", 13);
		Stuff s3 = s2;
		
		System.out.println(s1 == s2);
		System.out.println(s1.equals(s2));
		System.out.println(s1.stuff1.compareTo(s2.stuff1));
		
		System.out.println(s2 == s3);
		System.out.println(s2.equals(s3));
		System.out.println(s2.stuff1.compareTo(s3.stuff1));
	}

	private class Stuff {
		private String stuff1;
		private int stuff2;
		
		Stuff(String stuff1, int stuff2) {
			this.stuff1 = stuff1;
			this.stuff2 = stuff2;
		}
		
		public boolean equals(Stuff s) {
			return (stuff2 == s.stuff2);
		}
	}
}
