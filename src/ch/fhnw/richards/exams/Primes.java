package ch.fhnw.richards.exams;

public class Primes {

	/**
	 * Simple test program for the Sieve of Eratosthenes. The program times itself generating prime
	 * numbers up to the limit given as the first argument.
	 * 
	 * The program prints the total run-time (in milliseconds), as well as the number of primes found.
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			System.out.println("Usage: 'primes 99' to test numbers 1 to 99");
		} else {
			int roundLimit = Integer.parseInt(args[0]);
			long start = System.currentTimeMillis();
			boolean primes[] = sieveOfEratosthenes(roundLimit);
			long end = System.currentTimeMillis();
			long millis = end - start;
			int numPrimes = countPrimes(primes);

			long seconds = millis / 1000;
			millis = millis - seconds * 1000;
			String outStr = "Generated primes to " + roundLimit + " in ";
			outStr += seconds + "." + millis + " seconds";
			outStr += " and found " + numPrimes + " primes";
			System.out.println(outStr);
		}
	}

	private static boolean[] sieveOfEratosthenes(int roundLimit) {
		boolean primes[] = new boolean[roundLimit];

		// Initialize
		for (int i = 0; i < primes.length; i++) {
			primes[i] = true;
		}
		primes[0] = false;
		primes[1] = false;

		// Sieve
		for (int check = 0; check < primes.length / 2; check++) {
			if (primes[check]) {
				// found the next prime - mark all multiples in the array
				for (int flip = check * 2; flip < primes.length; flip += check) {
					primes[flip] = false;
				}
			}
		}
		return primes;
	}

	private static int countPrimes(boolean[] primes) {
		int numPrimes = 0;
		for (boolean p : primes) {
			if (p) numPrimes++;
		}
		return numPrimes;
	}
}
