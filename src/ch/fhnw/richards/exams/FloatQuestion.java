package ch.fhnw.richards.exams;

public class FloatQuestion {
  private float[] floats;

  public FloatQuestion(float[] floats_in) {
  	floats = new float[floats_in.length];
  	for (int i = 0; i < floats_in.length; i++) {
  		floats[i] = floats_in[i];
  	}
  }
  
  public String toString() {
  	String s = "[ ";
  	if (floats.length > 0) {
	  	s += Float.toString(floats[0]);
	  	for (int i = 1; i < floats.length; i++) {
	  		s += ", ";
	  		s += Float.toString(floats[i]);
	  	}
  	}
  	s += " ]";
  	return s;
  }
  
  public static void main(String[] args) {
  	float test[] = { 1.1f, 2.2f, 3.3f };
		FloatQuestion woof = new FloatQuestion(test);
		System.out.println(woof.toString());
	}

}
