package ch.fhnw.richards.exams.finalexam_2011;

public class Numerology {

	public static void main(String[] args) {
		String name = args[0];
		char[] characters = name.toCharArray();

		// Calculate the initial value of the name
		int value = 0;
		for (char c : characters) {
			value += letterValue(c);
		}

		// As long as the value is >= 10, add the digits to get a new value
		while (value >= 10) {
			value = sumOfDigits(value);
		}

		// Print the result
		System.out.println("You are a person of type " + value);
	}

	/**
	 * Berechnet die Summe der Ziffer in einem Integer
	 * 
	 * @param value Der Integer
	 * @return Die Summe der Ziffer
	 */
	private static int sumOfDigits(int value) {
		int sum = 0;
		while (value > 0) {
			sum += value % 10;
			value = value / 10;
		}
		return sum;
	}

	/**
	 * Berechnet den numerologischen Wert eines einzelnen Zeichen. Das Zeichen ist ein Buchstabe A-Z
	 * oder a-z, oder auch ein Leerschlag. Der Rückgabewert liegt im Bereich 1-9
	 * 
	 * @param c
	 *          Das Zeichen
	 * @return Ein Wert zwischen 1 und 9
	 */
	private static int letterValue(char c) {
		int value = 0;
		if (c != ' ') {
			if (c >= 'a') {
				value = c - 'a';
			} else {
				value = c - 'A';
			}
			value = value % 9 + 1;
		}
		return value;
	}

}
