package ch.fhnw.richards.exams.midterm_2007;

public class Pet {
	private String name;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name.length() >= 1) {
			this.name = name;
		}
	}
}
