package ch.fhnw.richards.exams.finalexam2010;

public class LychrelNumbers {
	private static long first;
	private static long last;

	public static void main(String[] args) {
		first = Long.parseLong(args[0]);
		last = Long.parseLong(args[1]);
		for (long test = first; test <= last; test++) {
			if (isLychrel(test)) {
				System.out.println("Lychrel number: " + test);
			}
		}
	}

	// Return true if "test" is a Lychrel number
	private static boolean isLychrel(long test) {
		boolean palindrome = false;
		for (int i = 0; !palindrome; i++) {
			long testReversed = reversed(test);
			test += testReversed;

			// If the sum is greater than Long.MAX_VALUE, we must stop
			if (test < 0)	break;

			// Test if we have a palindrome
			palindrome = isPalindrome(test);
		}
		return !palindrome;
	}

	// Return an integer with the order of digits reversed
	private static long reversed(long in) {
		long reversed = 0;
		while (in > 0) {
			long lastDigit = in % 10;
			reversed = 10 * reversed + lastDigit;
			in = in / 10;
		}
		return reversed;
	}

	// Return true if "test" is a palindrome
	private static boolean isPalindrome(long test) {
		return (test == reversed(test));
	}
}
