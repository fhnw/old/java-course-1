package ch.fhnw.richards.exams;

public abstract class Shape {
  public abstract double area();
  public abstract double circumference();
}
