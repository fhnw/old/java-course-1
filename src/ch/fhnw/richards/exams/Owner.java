package ch.fhnw.richards.exams;

public class Owner {
	private String name;
	private Pet pet;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		if (name.length() >= 1) {
			this.name = name;
		}
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}
}
