package ch.fhnw.richards.exams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Streams {

	public static void main(String[] args) {
		new Streams();
	}
	
	public Streams() {
		TestObject t1 = new TestObject(13);
		File f = new File("testfile");
		writeObject(t1, f);
		TestObject t2 = readObject(f);
		if (t1.equals(t2)) {
			System.out.println("Success!");
		} else {
			System.out.println("Failure!");
		}
	}
	
	private void writeObject(TestObject t, File f) {
		FileOutputStream streamOut;
		try {
			streamOut = new FileOutputStream(f);
			BufferedOutputStream streamBuff = new BufferedOutputStream(streamOut);
			ObjectOutputStream streamObj = new ObjectOutputStream(streamBuff);
			streamObj.writeObject(t);
			streamObj.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	private TestObject readObject(File f) {
		try {
			FileInputStream streamIn = new FileInputStream(f);
			BufferedInputStream streamBuff = new BufferedInputStream(streamIn);
			ObjectInputStream streamObj = new ObjectInputStream(streamBuff);
			TestObject t = (TestObject) streamObj.readObject();
			streamObj.close();
			return t;
		} catch (Exception e) {
			System.out.println(e.toString());
			return null;
		}
	}

}
class TestObject implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer a;
	private String b;
	public TestObject(Integer a) {
		this.a = a;
		this.b = a.toString();
	}
	public int getA() {
		return a;
	}
	public String getB() {
		return b;
	}
}
