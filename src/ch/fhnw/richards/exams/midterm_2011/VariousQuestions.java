package ch.fhnw.richards.exams.midterm_2011;

public class VariousQuestions {
	public static void main(String[] args) {
		String[] strings = { "aba", "cac", "abb", "dada" };
		// int result = whatResult(strings);
		// System.out.println(result);
	}

	// Count the 'a' characters in a string
	private static int whatResult(String input) {
		int count = 0;
		for (int i = 0; i < input.length(); i++) {
			char c = input.charAt(i);
			if (c == 'a') {
				count++;
			}
		}
		return count;
	}

	private static void woof() {
		String s = "asdf";
		if (s == "")
			woof();
		if (s.equals(""))
			woof();
		if (s.length() == 0)
			woof();
		if (s == null)
			woof();
	}

	
}
