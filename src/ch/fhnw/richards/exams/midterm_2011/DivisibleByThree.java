package ch.fhnw.richards.exams.midterm_2011;

import java.util.Scanner;

public class DivisibleByThree {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int in = scanner.nextInt();
		while (in > 0) {
			boolean answer = isDivisibleByThree(in);
			System.out.print("The number is ");
			if (!answer) System.out.print("not ");
			System.out.println("divisible by 3");
			
			in = scanner.nextInt();
		}
	}
	
	private static boolean isDivisibleByThree(int in) {
		boolean answer;
		if (in < 10) { // Base cases
			if (in == 3 | in == 6 | in == 9) {
				answer = true;
			} else {
				answer = false;
			}
		} else { // Inductive case
			int sum = sumOfDigits(in);
			answer = isDivisibleByThree(sum);
		}
		return answer;
	}
	
	private static int sumOfDigits(int in) {
		int sum = 0;
		while (in > 0) {
			int lastDigit = in % 10;
			sum += lastDigit;
			in = in / 10;
		}
		return sum;
	}
}
