package ch.fhnw.richards.exams.midterm_2011;

public class WhatDoesThisDo {

	public static void main(String[] args) {
		int start = 12;

		int[] result = new int[100];
		int numResults = -1;
		for (int i = 1; i <= start / 2; i++) {
			if ((start % i) == 0) {
				numResults++;
				result[numResults] = i;
			}
		}

		int answer = 0;
		for (int i = 0; i <= numResults; i++) {
			answer += result[i];
		}
		
		System.out.print(answer);
	}
}