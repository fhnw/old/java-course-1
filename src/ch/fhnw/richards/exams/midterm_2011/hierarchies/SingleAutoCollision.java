package ch.fhnw.richards.exams.midterm_2011.hierarchies;


/**
 * Model collision of a car with a stationary
 * object (tree, wall, etc.). The second object
 * is assumed to be 10x heavier than the car.
 */
public class SingleAutoCollision extends Collision {
	public SingleAutoCollision(int carWeight) {
		super(carWeight, 10*carWeight);
	}

	@Override
	public int getMaxPassengers1() {
		return MAX_CAR_PASSENGERS;
	}

	@Override
	public int getMaxPassengers2() {
		return 0;
	}
}
