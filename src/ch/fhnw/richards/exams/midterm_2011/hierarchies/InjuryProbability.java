package ch.fhnw.richards.exams.midterm_2011.hierarchies;

/**
 * This interface requires calculation of injury
 * probabilities based on collision speed
 */
public interface InjuryProbability {
	public void setCollisionSpeed(double speed);
	public double getInjuryProbability();
}
