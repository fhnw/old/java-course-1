package ch.fhnw.richards.exams.midterm_2011.hierarchies;

public abstract class Collision {
	public final static int MAX_CAR_PASSENGERS = 5;
	
	private int weight1;
	private int weight2;
	
	public abstract int getMaxPassengers1();
	public abstract int getMaxPassengers2();
	
	public int getWeight1() { return weight1; }
	public int getWeight2() { return weight2; }
	
	public Collision(int weight1, int weight2) {
		this.weight1 = weight1;
		this.weight2 = weight2;
	}
}
