package ch.fhnw.richards.exams.midterm_2011.hierarchies;

public class SingleAutoInjuries extends SingleAutoCollision implements InjuryProbability {
	private double collisionSpeed;
	
	public SingleAutoInjuries(int carWeight) {
		super(carWeight);
		collisionSpeed = 0;
	}

	@Override
	public void setCollisionSpeed(double speed) {
		if (speed < 0) speed = 0 - speed;
		this.collisionSpeed = speed;
	}
	
	@Override
	public double getInjuryProbability() {
		double tmpProb = collisionSpeed * collisionSpeed / 3000;
		if (tmpProb > 1) tmpProb = 1.0;
		return tmpProb;
	}

}
