package ch.fhnw.richards.exams.midterm_2011.Crashtest;

public abstract class Crashtest {
	private int weight1;
	private int weight2;
	
	public Crashtest(int weight1, int weight2) {
		this.weight1 = weight1;
		this.weight2 = weight2;
	}
	
	public int getWeight1() { return weight1; }
	public int getWeight2() { return weight2; }	

	public abstract int getSpeed1();
	public abstract int getSpeed2();
}

