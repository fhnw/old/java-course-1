package ch.fhnw.richards.exams.midterm_2011.Crashtest;

public class SingleCarCrashtest extends Crashtest {
	private int speed1;

	public SingleCarCrashtest(int weight1) {
		super(weight1, 10*weight1);
		speed1 = 50;
	}

	@Override
	public int getSpeed1() {
		return speed1;
	}
	
	public void setSpeed1(int speed) {
		this.speed1 = speed;
	}

	@Override
	public int getSpeed2() {
		return 0;
	}
}
