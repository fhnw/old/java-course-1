package ch.fhnw.richards.exams.midterm_2011.Crashtest;

public interface InjuryProbability {
	public void setCrashSpeed(int speed);
	public double getInjuryProbability();
}
