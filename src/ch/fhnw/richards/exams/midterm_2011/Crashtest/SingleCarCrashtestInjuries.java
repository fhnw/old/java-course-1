package ch.fhnw.richards.exams.midterm_2011.Crashtest;

public class SingleCarCrashtestInjuries extends SingleCarCrashtest implements InjuryProbability {

	public SingleCarCrashtestInjuries(int weight1) {
		super(weight1);
	}

	@Override
	public void setCrashSpeed(int speed) {
		if (speed < 0) speed = 0;
		setSpeed1(speed);
	}

	@Override
	public double getInjuryProbability() {
		double w = this.getSpeed1() * this.getSpeed1();
		w = w / 3000;
		if (w > 1.0) w = 1.0;
		return w;
	}

}
