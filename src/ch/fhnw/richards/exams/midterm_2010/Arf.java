package ch.fhnw.richards.exams.midterm_2010;

public class Arf {

	public static void main(String[] args) {
		for (int x = 2; x <= 10; x++) {
			int[] xResults = arf1(x);
			int xFinal = arf2(xResults);
			if (x == xFinal) {
				System.out.println(" " + x + " ");
			}
		}
	}
	
	private static int[] arf1(int in) {
		int[] tmpResult = new int[100];
		int numResults = -1;
		for (int i = 1; i <= in/2; i++) {
			if ( (in % i) == 0) {
				numResults++;
				tmpResult[numResults] = i;
			}
		}
		
		int[] result = new int[numResults+1];
		for (int i = 0; i <= numResults; i++) {
			result[i] = tmpResult[i];
		}
		return result;
	}

	private static int arf2(int[] in) {
		int result = 0;
		for (int i = 0; i < in.length; i++) {
			result += in[i];
		}
		return result;
	}	
}
