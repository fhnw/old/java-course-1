package ch.fhnw.richards.exams.midterm_2010;

public class Oink {

	public static void main(String[] args) {
		long lastExactValue = 0;
		float f;
		long i, j;
		for (i = 1; i < Long.MAX_VALUE; i++) {
			f = i;
			
			// Convert back to long - see if the value is correct
			j = (long) f;
			if (j == i) { // this value can be represented exactly
				if ((i - lastExactValue) >= 100) { // Found gap of 100
					System.out.println(lastExactValue + ", " + i);
					break;
				}
				lastExactValue = i;
			}
		}
	}

}
