package ch.fhnw.richards.exams.midterm_2010;

public class Woof {

	public static void main(String[] args) {
		String[] result;
		boolean allOk = false;

		if (args.length >=1) {
			allOk = true;
			for (int i = 1; i < args.length; i++) {
				if (args[i].length() != args[0].length()) {
					allOk = false;
				}
			}
		}
		
		if (!allOk) {
			System.out.println("Error: lengths are not the same");
		} else {
			result = processStrings(args);
			for (int i = 0; i < result.length; i++) {
				System.out.println(result[i]);
			}
		}
	}

	private static String[] processStrings(String[] stringsIn) {
		int numberOfResultStrings = stringsIn[0].length();
		String[] result = new String[stringsIn[0].length()];
		
		for (int i = 0; i < numberOfResultStrings; i++) {
			StringBuffer tmpString = new StringBuffer();
			for (int j = 0; j < stringsIn.length; j++) {
				tmpString.append(stringsIn[j].charAt(i));
			}
			result[i] = tmpString.toString();
		}
		return result;
	}	
}
