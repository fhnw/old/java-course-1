package ch.fhnw.richards.exams;

public class ChessGame {
	private ChessPiece[] pieces;
	
	public ChessGame() {
		pieces = new ChessPiece[32];
		for (int i = 1; i < 9; i++) {
			char col = (char) (((int) 'a') + i - 1);
			pieces[i] = new ChessPiece(0, true, 2, col);
			pieces[i+16] = new ChessPiece(0, false, 2, col);
		}
	}
	
	private class ChessPiece {
		private int pieceType;
		private boolean isWhite;
		private int row;
		private char col;
		
		private ChessPiece(int pieceType, boolean isWhite, int row, char col) {
			this.pieceType = pieceType;
			this.isWhite = isWhite;
			this.row = row;
			this.col = col;
		}
		
		private boolean onWhiteSquare() {
			int colAsInt = ((int) col) + 1 - ((int) 'a');
			int sum = row + col;
			// For white squares, the sum is odd (mod 2 != 0)
			return ((sum % 2) != 0);
		}
	}
}
