package ch.fhnw.richards.exams;

import java.util.ArrayList;

public class DogTest {
	// Declare an ArrayList to hold the dogs
	private ArrayList<Dog> dogs;

	public static void main(String[] args) {
		new DogTest();
	}
	
	private DogTest() {
		Dog thisDog;
		
		// Put the dogs into the ArrayList
		dogs = new ArrayList<Dog>();
		thisDog = new Dog("Fluffy", "Poodle");
		dogs.add(thisDog);
		thisDog = new Dog("Woofy", "Dachshund");
		dogs.add(thisDog);
		
		// print the dogs
		for( Dog d : dogs ) {
			System.out.println(d.getName() + " is a " + d.getBreed());
		}
	}
	
	// Declare the private inner class Dog
	private class Dog {
		private String name;
		private String breed;
		private Dog(String name, String breed) {
			this.name = name;
			this.breed = breed;
		}		
		public String getName() {
			return name;
		}
		public String getBreed() {
			return breed;
		}
	}
}
