package ch.fhnw.richards.exams;

public interface Positioned {
  public void setPosition(double x, double y);
  public double getX();
  public double getY();
}
