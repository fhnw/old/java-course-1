package ch.fhnw.richards.exams;

import java.io.IOException;

public class Woof5 {

	public static void main(String[] args) {
		String[] inStrings = new String[10];
		int numStrings = 0;
		
		// Read the strings
		try {
			for (int i = 0; i < 10; i++) {
				inStrings[numStrings] = readline();
				if (inStrings[numStrings].length() == 0) break;
				numStrings++;
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		
		// Process the strings
		String output = "";
		for (int i = 0; i < numStrings; i++) {
			int charPosition = i % inStrings[i].length();
			char c = inStrings[i].charAt(charPosition);
			output += c;
		}
		
		// Write the answer
		System.out.println("Result: \"" + output + "\"");
	}

	public static String readline() throws IOException {
		boolean lineDone = false;
		String stringIn = "";
		
		while (!lineDone) {
			int c = System.in.read();
			lineDone = ( (c == -1) | (c == 13) ); // no data or 'cr'
			if (!lineDone) {
				if (c != 10) { // ignore 'lf'
					stringIn += (char) c;
				}
			}
		}
		return stringIn;
	}
}
