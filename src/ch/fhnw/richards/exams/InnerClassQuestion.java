package ch.fhnw.richards.exams;

public class InnerClassQuestion {
	private Piece[] white = new Piece[16];
	private Piece[] black = new Piece[16];
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		InnerClassQuestion game = new InnerClassQuestion();
		game.go();
	}
	
	private InnerClassQuestion() {
		// 8 pawns
		for (int i = 0; i < 8; i++) {
			
			white[i] = new Piece(0);
			black[i] = new Piece(0);
		}
		// 2 rooks
		for (int i = 8; i <= 9; i++) {
			white[i] = new Piece(1);
			black[i] = new Piece(1);
		}
	}
	
	private void go() {
		
	}
	
	/**
	 * Inner class represents pieces
	 * 0 = pawn, 1 = rook, 2 = knight
	 * 3 = bishop, 4 = queen, 5 = king
	 */
	private class Piece {
		private int pieceType;
		private char row;
		private int column;
		private Piece(int pieceType) {
			
		}
	}

}
