package ch.fhnw.richards.exams;

public class AnimalTest {

	public static void main(String[] args) {
		Horse h = new Horse();
		System.out.println("method1 returns: " + Animal.method1());
		System.out.println("method1 returns: " + Horse.method1());
		System.out.println("method1 returns: " + h.method1());
		System.out.println("method2 returns: " + h.method2());
	}

}
