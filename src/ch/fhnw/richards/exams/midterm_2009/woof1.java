package ch.fhnw.richards.exams.midterm_2009;

public class woof1 {

	public static void main(String[] args) {
		int consonants = 0; // Consonants are b, d, f, g, h, j, etc.
		int vowels = 0; // vowels are a, e, i, o and u
		int other = 0; // punctuation, spaces, etc.
		// For each argument in the array
		for (int i = 0; i < args.length; i++) {
			// Convert the entire string to lower case (ABC --> abc)
			String tmpStr = args[i].toLowerCase();
			
			// Convert the string to an array of characters
			char[] strCharacters = tmpStr.toCharArray();
			
			// For each character in the array
			for (int j = 0; j < strCharacters.length; j++) {
				Character tmpChar = strCharacters[j];
				// Is it a letter?
				if (Character.isLetter(tmpChar)) {
					// Is it a vowel?
					if (tmpChar.equals('a') | tmpChar.equals('e') | tmpChar.equals('i') | tmpChar.equals('o') | tmpChar.equals('u')) {
						vowels++;
					} else {
						consonants++;
					}
				} else {
					other++;
				}				
			}
		}
		
		// Print input strings
		for (int i = 0; i < args.length; i++) {
			System.out.println(args[i]);
		}
		
		// Print results
		System.out.println("Consonants, vowels, other: " + consonants + ", " + vowels + ", " + other);
	}

}
