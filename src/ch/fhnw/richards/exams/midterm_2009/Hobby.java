package ch.fhnw.richards.exams.midterm_2009;

public class Hobby {
	private String name;
	
	public String getName() {
		return name;
	}

	public Hobby(String name) throws Exception {
		// Name may not be empty, otherwise we throw an exception!
		if (name.length() >= 1) {
			this.name = name;
		} else {
			throw new Exception ("Name may not be empty");
		}
	}
}
