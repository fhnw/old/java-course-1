package ch.fhnw.richards.exams.midterm_2009;

public class Person {
	private static int last_id = 0;
	
	private int id;
	private String lastname;
	private String firstname;
	private Hobby hobby;

	public Person() {
		id = last_id++;
	}
	
	public int getId() {
		return id;
	}
	
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String name) {
		// Only set name if it meets our requirements
		if (name != null && name.length() >= 1) {
			lastname = name;
		}
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String name) {
		// Only set name if it meets our requirements
		if (name != null && name.length() >= 1) {
			firstname = name;
		}
	}

	public Hobby getHobby() {
		return hobby;
	}

	public void setHobby(Hobby hobby) {
		this.hobby = hobby;
	}
}
