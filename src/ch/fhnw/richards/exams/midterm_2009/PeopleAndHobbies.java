package ch.fhnw.richards.exams.midterm_2009;

import java.io.IOException;

public class PeopleAndHobbies {
	private static PeopleAndHobbies mainApp; // Singleton
	private Person[] people;

	public static void main(String[] args) {
		mainApp = new PeopleAndHobbies();
		mainApp.readData();
		mainApp.printData();
	}

	private PeopleAndHobbies() {
		people = new Person[3];
	}

	private void readData() {
		String line;
		int linesRead = 0;
		while (linesRead < 3) {
			try {
				line = readline();

				String[] data = line.split("/"); // Split name and hobby
				if (data.length > 0 & data.length < 3) {
					for (int i = 0; i < data.length; i++) {
						data[i] = data[i].trim();
					}
					if (data[0].length() > 0) {
						people[linesRead] = new Person();
						String[] names = data[0].split(" "); // Split first and last name
						for (int i = 0; i < names.length; i++) {
							names[i] = names[i].trim();
						}
						people[linesRead].setFirstname(names[0]);
						people[linesRead].setLastname(names[1]);
						if (data.length > 1 && data[1].length() > 0) {
							Hobby pet = new Hobby(data[1]);
							people[linesRead].setHobby(pet);
						}
						linesRead++;
					}
				}
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}
	}

	private void printData() {
		for (Person p : people) {
			System.out.print("Person " + p.getFirstname() + " " + p.getLastname() + " does ");
			Hobby h = p.getHobby();
			if (h != null) {
				System.out.println(h.getName());
			} else {
				System.out.println("nothing");
			}
		}
	}

	public static String readline() throws IOException {
		boolean lineDone = false;
		String stringIn = "";

		while (!lineDone) {
			int c = System.in.read();
			lineDone = ((c == -1) | (c == 10) | (c == 13)); // no data or 'cr'
			if (!lineDone) {
				if (c != 10) { // ignore 'lf'
					stringIn += (char) c;
				}
			}
		}
		return stringIn;
	}
}
