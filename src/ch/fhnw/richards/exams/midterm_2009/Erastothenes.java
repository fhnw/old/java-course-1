package ch.fhnw.richards.exams.midterm_2009;

public class Erastothenes {
	public static void main(String[] args) {
		int[] numbers = new int[99];
		
		// Initialize to zero
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = 0;
		}
		
		// Check each number
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == 0) { // found a prime
				int prime = i+2;
				// Mark all multiples of the prime
				for (int j = i+prime; j < numbers.length; j+=prime) {
					numbers[j] = 1;
				}
			}
		}

		// Print the results
		System.out.print("Primes: ");
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] == 0) {
				System.out.print(i+2 + ", ");
			}
		}
		System.out.println();
	}
}
