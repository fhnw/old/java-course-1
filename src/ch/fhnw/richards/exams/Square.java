package ch.fhnw.richards.exams;

public class Square extends Shape {
  private double width;
  public Square(double width) {
  	this.width = width;
  }
  public double getWidth() {
  	return width;
  }
  public double area() {
  	return width * width;
  }
  public double circumference() {
  	return 4 * width;
  }
}
