package ch.fhnw.richards.exams.midterm_2008;

public class Pet {
	private String name;
	
	public String getName() {
		return name;
	}

	public Pet(String name) throws Exception {
		// Name may not be empty � otherwise we throw an exception!
		if (name.length() >= 1) {
			this.name = name;
		} else {
			throw new Exception ("Name may not be empty");
		}
	}
}
