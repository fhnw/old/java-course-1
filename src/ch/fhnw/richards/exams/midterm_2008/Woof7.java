package ch.fhnw.richards.exams.midterm_2008;

public class Woof7 {

	public static void main(String[] args) {
		int nums[] = new int[args.length];
		for (int i = 0; i < args.length; i++) {
			nums[i] = Integer.parseInt(args[i]);
		}

		int sum = 0;
		long product = 1;
		for (int num : nums) {
			sum += num;
			product *= num;
		}
		float average = (float) sum / nums.length;

		System.out.println("Sum = " + sum +
				", Product = " + product +
				", Average = " + average);
	}
}
