package ch.fhnw.richards.exams.midterm_2008;

public class Woof3 {

	public static void main(String[] args) {
		String[] args2 = {"abc", "bcd", "cde", "def", "efg"};
		char tmpValue;
		for (int i = 0; i < args2.length; i++) {
			if (args2[i].compareTo("c") < 0) {
				tmpValue = args2[i].charAt(0);
				tmpValue += 2;
				args2[i] = Character.toString(tmpValue);
			} else if (args2[i].compareTo("c") > 0) {
				tmpValue = args2[i].charAt(2);
				tmpValue -= 1;
				args2[i] = Character.toString(tmpValue);
			} else {
				tmpValue = args2[i].charAt(1);
				args2[i] = Character.toString(tmpValue);
			}
		}
		String out = "";
		for (String s : args2) {
			out += s;
		}
		System.out.println(out);
	}

}
