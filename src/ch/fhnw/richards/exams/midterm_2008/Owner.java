package ch.fhnw.richards.exams.midterm_2008;

public class Owner {
	private String name;
	private Pet pet;
	
	public String getName() {
		return name;
	}

	public Owner(String name) throws Exception {
		// Name may not be empty � otherwise we throw an exception!
		if (name.length() >= 1) {
			this.name = name;
		} else {
			throw new Exception("Name may not be empty");
		}
	}

	public Pet getPet() {
		return pet;
	}

	public void setPet(Pet pet) {
		this.pet = pet;
	}
}
