package ch.fhnw.richards.exams.midterm_2008;

public class Thingifier {
	public static void main(String[] args) {
		Thingifier thingifier = new Thingifier(args);
	}

	public Thingifier(String[] args) {
		int total = 0;
		for (int i = 0; i < args.length; i++) {
			Thing thing = new Thing(args[i]);
			total += thing.getValue();
		}
		System.out.println("Total: " + total);
	}

	private class Thing {
		String thingString;

		public Thing(String in) {
			thingString = in;
		}

		public int getValue() {
			char[] chars = thingString.toCharArray();
			int value = 0;
			for (char c : chars) {
				if (Character.isDigit(c)) {
					value += Character.getNumericValue(c);
				}
			}
			return value;
		}
	}
}
