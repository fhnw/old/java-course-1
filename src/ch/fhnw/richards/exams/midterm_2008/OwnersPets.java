package ch.fhnw.richards.exams.midterm_2008;

import java.io.IOException;

public class OwnersPets {
	private static OwnersPets mainApp;
	private Owner[] owners;
	
	public static void main(String[] args) {
		mainApp = new OwnersPets();
		mainApp.readData();
		mainApp.printData();
	}

	private OwnersPets() {
		owners = new Owner[3];
	}
	
	private void readData() {
		String line;
		int linesRead = 0;
			while (linesRead < 3) {
				try {
					line = readline();
					
					String[] names = line.split("/");
					if (names.length > 0 & names.length < 3) {
						for (int i = 0 ; i < names.length ; i++) {
							names[i] = names[i].trim();
						}
						if (names[0].length()> 0) {
							owners[linesRead] = new Owner(names[0]);

							if (names.length > 1 && names[1].length() > 0) {
								Pet pet = new Pet(names[1]);
								owners[linesRead].setPet(pet);
							}
							linesRead++;
						}
					}							
				} catch (Exception e) {
					System.out.println(e.toString());
				}
			}
	}
	
	private void printData() {
		for (Owner o : owners) {
			System.out.print("Person " + o.getName() + " owns ");
			Pet p = o.getPet();
			if (p != null) {
				System.out.println(p.getName());
			} else {
				System.out.println("no pet");
			}
		}
	}
	
	public static String readline() throws IOException {
		boolean lineDone = false;
		String stringIn = "";
		
		while (!lineDone) {
			int c = System.in.read();
			lineDone = ( (c == -1) | (c == 13) ); // no data or 'cr'
			if (!lineDone) {
				if (c != 10) { // ignore 'lf'
					stringIn += (char) c;
				}
			}
		}
		return stringIn;
	}
}
