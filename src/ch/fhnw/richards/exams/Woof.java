package ch.fhnw.richards.exams;

public class Woof {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String s1 = "asdf";
		String s2 = "asd";
		String s3 = "asdf";
		String s4 = s3;
		s2 += "f";
		
		/* all strings now contain 'asdf' */
		
		System.out.print(s1 == s2);
		System.out.print(s1 == s3);
		System.out.print(s1 == s4);
		System.out.print(s2 == s3);
		System.out.print(s2 == s4);
		System.out.print(s3 == s4);
		
		System.out.print(s1.equals(s2));
		System.out.print(s1.equals(s3));
		System.out.print(s1.equals(s4));
		System.out.print(s2.equals(s3));
		System.out.print(s2.equals(s4));
		System.out.print(s3.equals(s4));

	}

}
