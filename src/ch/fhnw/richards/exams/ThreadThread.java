package ch.fhnw.richards.exams;

public class ThreadThread extends Thread {
	ThreadCounter counter;
	boolean increment;

	public ThreadThread(ThreadCounter c, boolean i) {
		counter = c;
		increment = i;
	}

	@Override
	public void run() {
		for (int i = 0; i < 1000000; i++) {
			if (increment) {
				counter.increment();
			} else {
				counter.decrement();
			}
		}
	}
}
