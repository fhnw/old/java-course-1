package ch.fhnw.richards.exams;

public class ThreadTest {

	public static void main(String[] args) {
		// Create the counter object
		ThreadCounter c = new ThreadCounter();

		// Create two threads: one to increment one million times
		// and one to decrement one million times. The value at
		// the end should always be zero.
		ThreadThread incrementer = new ThreadThread(c, true);
		ThreadThread decrementer = new ThreadThread(c, false);

		// start the threads
		incrementer.start();
		decrementer.start();

		// Wait for the threads to finish
		try {
			incrementer.join();
			decrementer.join();
		} catch (Exception e) {
			System.out.println("This will not happen");
		}
		
		// Print the final value
		System.out.println("The final value is " + c.toString());
	}
}
