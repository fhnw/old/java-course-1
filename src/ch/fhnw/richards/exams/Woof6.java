package ch.fhnw.richards.exams;

import java.io.IOException;

public class Woof6 {
	private static Woof6 mainApp;
	private Owner[] owners;
	
	public static void main(String[] args) {
		mainApp = new Woof6();
		mainApp.readData();
		mainApp.sortData();
		mainApp.printData();
	}

	private Woof6() {
		owners = new Owner[3];
	}
	
	private void readData() {
		String name;
		try {
			for (int i = 0; i < owners.length; i++) {
				name = readline();
				if (name.length() == 0) {
					throw new Exception("Missing data!");
				}
				owners[i] = new Owner();
				owners[i].setName(name);
				name = readline();
				if (name.length() > 0) {
					Pet pet = new Pet();
					pet.setName(name);
					owners[i].setPet(pet);
				}
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	private void sortData() {
		for (int i = 0; i < (owners.length-1); i++) {
			int smallest = i;
			for (int j = i+1; j < owners.length; j++) {
				String name_smallest = owners[smallest].getName();
				String name_test = owners[j].getName();
				if (name_smallest.compareTo(name_test) > 0) {
					smallest = j;
				}
			}
			if (smallest != i) {
				Owner tmp = owners[i];
				owners[i] = owners[smallest];
				owners[smallest] = tmp;
			}
		}
	}
	
	private void printData() {
		for (Owner o : owners) {
			Pet p = o.getPet();
			if (p != null) {
				System.out.println(p.getName());
			}
		}
	}
	
	public static String readline() throws IOException {
		boolean lineDone = false;
		String stringIn = "";
		
		while (!lineDone) {
			int c = System.in.read();
			lineDone = ( (c == -1) | (c == 13) ); // no data or 'cr'
			if (!lineDone) {
				if (c != 10) { // ignore 'lf'
					stringIn += (char) c;
				}
			}
		}
		return stringIn;
	}
}
