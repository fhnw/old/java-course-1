package ch.fhnw.richards.exams;

public class PositionedCircle extends Circle implements Positioned {
	private double x;
	private double y;

	public PositionedCircle(double radius) {
		super(radius);
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void setPosition(double x, double y) {
		this.x = x;
		this.y = y;
	}	
}
