package ch.fhnw.richards.exams.exam2012_1;

public class Rectangle {

	public class Point {
		private int x,y;
		public Point(int x, int y) {this.x = x; this.y = y;}
		public int getX() { return x;}
		public int getY() { return y;}
	}
	
	private Point origin;
	private int width, height;
	
	public Rectangle(Point p, int w, int h) {
		origin = p;
		width = w;
		height = h;
	}
	
	public Rectangle(int w, int h) {
		this(null, w, h);
		origin = new Point(0,0);
	}
}
