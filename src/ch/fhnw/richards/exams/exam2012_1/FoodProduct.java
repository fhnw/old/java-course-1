package ch.fhnw.richards.exams.exam2012_1;

import java.util.Date;

public class FoodProduct extends Product {
	private boolean perishable;
	private Date expiryDate;
	
	public FoodProduct(int productID, String name) {
		super(productID, name);
		perishable = false;
	}
	
	public FoodProduct(int productID, String name, Date expiryDate) {
		super(productID, name);
		perishable = true;
		this.expiryDate = expiryDate;
	}

	@Override
	public float getShippingCost() {
		float shippingCost = super.getShippingCost();
		if (perishable) shippingCost += 20;
		return shippingCost;
	}

	public boolean isPerishable() {
		return perishable;
	}

	public void setPerishable(boolean perishable) {
		this.perishable = perishable;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
}
