package ch.fhnw.richards.exams.exam2012_1;

public class Product {
	private int productID;
	private String name;
	private String description = "";
	private float weight = 0;
	private float price = 0;
	
	public Product(int productID, String name) {
		this.productID = productID;
		this.name = name;
	}
	
	public float getShippingCost() {
		float shippingCost = 10;
		if (weight > 20) {
			shippingCost = 20;
		} else if (weight > 10) {
			shippingCost = 15;
		}
		return shippingCost;
	}

	public int getProductID() {
		return productID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

}
