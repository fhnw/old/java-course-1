package ch.fhnw.richards.exams.exam2012_1;

import java.util.Scanner;

public class FreqAnalysis {
	private static String encryptedString; // input text as string
	private static char[] encryptedChars; // input text as char[]
	private static int[] alphabet; // results of frequency analysis

	public static void main(String[] args) {
		// Create the alphabet array
		alphabet = new int[26]; // omit right-hand side of equals

		// Initialize the alphabet array to all zeroes
		for (int i = 0; i < alphabet.length; i++) { // omit inside of for loop
			alphabet[i] = 0;
		}

		readInputText();
		analyzeInput();
		presentResults();
	}

	/**
	 * readInputText reads an encoded string from the console, and converts it
	 * to lower case
	 */
	private static void readInputText() {
		Scanner s = new Scanner(System.in);
		String inputString = s.nextLine();
		encryptedString = inputString.toLowerCase();
	}

	/**
	 * analyseInput converts the input text to a character-array, and then
	 * counts the occurrences of each character. The counters for the individual
	 * letters are the elements of the "alphabet" array.
	 */
	private static void analyzeInput() {
		// Convert to an array of characters
		encryptedChars = encryptedString.toCharArray(); // omit right-hand side

		// Count characters in encryptedChars
		// Be sure to ignore characters that are not letters
		for (char c : encryptedChars) {
			if (c >= 'a' & c <= 'z') {
				int index = (int) c - (int) 'a'; // omit contents of loop
				alphabet[index]++;
			}
		}
	}

	/**
	 * The letter with the highest count is 'e', the letter with the
	 * second-highest count is 't'
	 */
	private static void presentResults() {
		int highestIndex;
		int secondHighestIndex;

		// Initialize using the first two elements of the array
		if (alphabet[0] > alphabet[1]) {
			highestIndex = 0;
			secondHighestIndex = 1;
		} else {
			highestIndex = 1;
			secondHighestIndex = 0;
		}

		// Search through the rest of the array
		for (int index = 2; index < alphabet.length; index++) {
			int thisCount = alphabet[index]; // omit contents of loop
			if (thisCount > alphabet[highestIndex]) {
				secondHighestIndex = highestIndex;
				highestIndex = index;
			} else if (thisCount > alphabet[secondHighestIndex]) {
				secondHighestIndex = index;
			}
		}

		// Convert indices to characters
		char letterHighestIndex = (char) ((int) 'a' + highestIndex);
		char letterSecondHighestIndex = (char) ((int) 'a' + secondHighestIndex);

		// Print results to console
		System.out.println("'e' is encoded as " + letterHighestIndex);
		System.out.println("'t' is encoded as " + letterSecondHighestIndex);
	}
}
