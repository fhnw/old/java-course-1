package ch.fhnw.richards.exams;

public class ThreadCounter {
	private int value = 0;

	public void increment() {
		int tmpValue = value;             // Do not change
		for (int i = 0; i < 1000; i++) {  // Do not change
			tmpValue++;                     // Do not change
		}                                 // Do not change
		for (int i = 0; i < 999; i++) {   // Do not change
			tmpValue--;                     // Do not change
		}                                 // Do not change
		value = tmpValue;                 // Do not change
	}

	public void decrement() {
		int tmpValue = value;             // Do not change
		for (int i = 0; i < 999; i++) {   // Do not change
			tmpValue++;                     // Do not change
		}                                 // Do not change
		for (int i = 0; i < 1000; i++) {  // Do not change
			tmpValue--;                     // Do not change
		}                                 // Do not change
		value = tmpValue;                 // Do not change
	}

	public String toString() {
		return Integer.toString(value);
	}

}