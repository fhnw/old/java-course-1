package ch.fhnw.richards.exams.astronomy;

public class Star extends AstroBody {

	public Star() {
		name = "My star";
		diameter = 0;
		mass = 0;
		orbiting = null;
	}

}
