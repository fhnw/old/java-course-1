package ch.fhnw.richards.exams.astronomy;

public class AstroBody {
  public static final double G = 6.67428E-11; // Grav. constant
  public String name; // Catalog number or unique name
  protected double diameter; // in meters
  double mass; // in kg
  protected AstroBody orbiting; // object orbited (null for stars)

  public AstroBody() {
    name = "My AstroBody";
    diameter = 0;
    mass = 0;
    orbiting = null;
  }
}