package ch.fhnw.richards.lecture05;

import java.util.ArrayList;
import java.util.Scanner;

public class Family {
	// The main method does nothing but create
	// the first object: the user interface.
	private static Family mainProgramObject;

	// This ArrayList keeps a list of all the
	// people in our family.
	private ArrayList<Person> familyMembers;

	// The scanner to read from the console
	Scanner scanIn;

	/**
	 * The main method is nearly empty – it creates the first object, and then
	 * calls the method to read and execute commands.
	 */
	public static void main(String[] args) {
		mainProgramObject = new Family();
		mainProgramObject.runCommands();
	}

	/**
	 * Constructor for this class
	 */
	private Family() {
		// Initialize the list of family members
		familyMembers = new ArrayList<Person>();

		// Initialize the scanner
		scanIn = new Scanner(System.in);
	}

	/**
	 * Loop: print menu, read command, execute command. Stop when the command
	 * "stop"
	 */
	private void runCommands() {
		String command = "";
		String personName;

		while (!command.equals("stop")) {
			printCommandMenu();
			command = scanIn.next();

			// all commands except "stop" and "all"
			// require the name of a person
			if (command.equals("all")) {
				printAll();
			} else if (!command.equals("stop")) {
				personName = scanIn.next();
				if (command.equals("add")) {
					addPerson(personName);
				} else if (command.equals("grandparents")) {
					getGrandparents(personName);
				}
			}
		}
	}

	/**
	 * Print out the menu of available commands
	 */
	private void printCommandMenu() {
		System.out.println("\nEnter one of the following");
		System.out.println("  stop - end the program");
		System.out.println("  add <name> - add a new person to the family");
		System.out
				.println("  grandparents <name> - print the grandparents of this person");
		System.out.println("  all - print all people who have been entered");
		System.out.println("Note: use first names only, no spaces!");
		System.out.print("\nCommand? ");
	}

	/**
	 * Add a new person to the family. We read in the names of this person's
	 * parents. Any names entered <b>must already exist</b> in the family.
	 * 
	 * Enhancement: only add a new person to the family if they are not already
	 * present (i.e., guarantee unique names)
	 */
	private void addPerson(String name) {
		String input;
		Person newPerson = new Person(name);

		// Gender
		System.out.print("Enter gender ('m' or 'f'): ");
		input = scanIn.next();
		if (input.equals("m")) {
			newPerson.setMale();
		} else {
			newPerson.setFemale();
		}

		// Father
		System.out.print("Enter the father's name, or 'none': ");
		input = scanIn.next();
		if (!input.equals("none")) {
			Person father = Person.findPerson(familyMembers, input);
			if (father != null) newPerson.setFather(father);
		}

		// Mother
		System.out.print("Enter the mother's name, or 'none': ");
		input = scanIn.next();
		if (!input.equals("none")) {
			Person mother = Person.findPerson(familyMembers, input);
			if (mother != null) newPerson.setMother(mother);
		}

		// Add the new person to the family
		familyMembers.add(newPerson);
	}

	/**
	 * Print a list of all people stored
	 * 
	 * Note: Does not work as expected, because we have yet not implemented the
	 * toString() method in Person
	 * 
	 */
	private void printAll() {
		for (Person p : familyMembers) {
			System.out.println(p.toString());
		}
	}

	/**
	 * Print the grandparents of the person
	 * 
	 * Error checking should be added: what happens if mothers and/or fathers do
	 * not exist?
	 * 
	 * @param name
	 *            the name of the person
	 */
	private void getGrandparents(String name) {
		Person thisPerson = Person.findPerson(familyMembers, name);

		ArrayList<Person> grandparents = thisPerson.getGrandparents();

		System.out.print("The grandparents are: ");
		for (Person gp : grandparents) {
			System.out.print(gp.getName() + ", ");
		}
		System.out.println();
	}
}
