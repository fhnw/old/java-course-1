package ch.fhnw.richards.astronomy;

import java.util.ArrayList;

public class Planet extends AstroBody {
	ArrayList<Planet> moons;
	ArrayList<String> atmosphericGases;

  public Planet(String name) {
  	super(name);
  	atmosphericGases = new ArrayList<String>();
  }
  
  /**
   * Calculate surface gravity from mass and diameter
   */
  public double getGravity() {
  	return G * mass / (diameter * diameter / 4);
  }
  
  public void addMoon(Planet p) {
  	moons.add(p);
  }
  public ArrayList<Planet> getMoons() {
  	return moons;
  }
  public void addAtmospericGas(String g) {
  	atmosphericGases.add(g);
  }
  public ArrayList<String> getAtmosphericGases() {
  	return atmosphericGases;
  }
}
