package ch.fhnw.richards.astronomy;

import java.util.ArrayList;

public class Star extends AstroBody {
	String starType;
	ArrayList<Planet> planets;
  public Star(String name) {
  	super(name);
  	planets = new ArrayList<Planet>();
  }
  
  public Star(String name, String starType) {
  	super(name);
  	this.starType = starType;
  	planets = new ArrayList<Planet>();
  }
  
  public double getGravity() {
  	return G * mass / (diameter * diameter / 4);
  }

  public String getStarType() {
  	return starType;
  }
  public void setStarType(String starType) {
  	this.starType = starType;
  }
  public void addPlanet(Planet p) {
  	planets.add(p);
  }
  public ArrayList<Planet> getPlanets() {
  	return planets;
  }
}
