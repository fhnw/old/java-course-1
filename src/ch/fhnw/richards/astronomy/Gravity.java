package ch.fhnw.richards.astronomy;

public interface Gravity {
	public double getGravity();

}
