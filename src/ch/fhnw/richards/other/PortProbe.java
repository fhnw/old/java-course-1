package ch.fhnw.richards.other;

import java.net.ConnectException;

public class PortProbe {

	public static void main(String[] args) {
		try {
			String ipAddress = getIpAddress(args);
			int portNumber = getPortNumber(args);
		
			// Check the selected port-number
			try {
				// Try to connect
				java.net.Socket sock = new java.net.Socket(ipAddress, portNumber);
				// Report success (vulnerability)
				System.out.println("Port " + portNumber + " is open");
				// Close the connection
				sock.close();
			} catch (ConnectException e) { // Failure causes a connect-exception
				System.out.println("Port " + portNumber + " is not open");
			}
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
	
	// Read IP-address from the command-line
	public static String getIpAddress(String[] args) throws Exception {
		boolean formatOK = false;
		if (args.length >= 1) {
			// Check for validity (not complete, but not bad)
			String ipPieces[] = args[0].split("\\."); // Must escape (see documentation)
			// Must have 4 parts
			if (ipPieces.length == 4) {
				// Each part must be an integer 0 to 255
				formatOK = true; // set to false on the first error
				int byteValue = -1;
				for (String s : ipPieces) {
					byteValue = Integer.parseInt(s); // may throw NumberFormatException
					if (byteValue < 0 | byteValue > 255) formatOK = false;
				}
			}
			// Do we have a valid IPv4 address?
			if (!formatOK) {
				throw new Exception("Invalid IPv4 address");
			}
		} else {
  		throw new Exception("Usage: java Woof <host> <port>");
		}
		return args[0];
	}
	
	// Read port number from the command-line
	public static int getPortNumber(String[] args) throws Exception {
		int portNumber = 0;
		boolean formatOK = false;
		if (args.length >= 2) {
			try {
				portNumber = Integer.parseInt(args[1]);				
				if (portNumber >= 0 & portNumber <= 65535) {
					formatOK = true;
				}				
			} catch (NumberFormatException e) {
			}
			
			if (!formatOK) {
				throw new Exception("Port must be an integer 0-65535");
			}
		} else {
  		throw new Exception("Usage: java Woof <host> <port>");
		}
		return portNumber;
	}	
}
