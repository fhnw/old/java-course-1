package ch.fhnw.richards.other;
import java.util.logging.*;

public class Lecture4_array_example {
	private static Logger logger = Logger.getLogger("ch.fhnw.richards");
	public static void main(String[] args) {
		int[] result;
		int[] intArgs = new int[args.length];
		
		logger.setLevel(Level.OFF); // Set desired level
		
		for (int i = 0; i < args.length; i++) {
			logger.info("Processing argument '" + args[i] + "'");
			intArgs[i] = Integer.parseInt(args[i]);
		}
		result = processArray(intArgs);
		System.out.print("The result is: [");
		for (int i : result) {
			System.out.print(i + ", ");
		}
		System.out.println("]");
	}
	
  public static int[] processArray(int[] arrayIn) {
  	int numNewElements = (arrayIn.length+1)/2;
  	int[] result = new int[numNewElements];
  	for (int i = 0; i < numNewElements; i++) {
  		int j = arrayIn.length - i - 1;
  		assert (j >= 0) & (j < arrayIn.length);
  		result[i] = arrayIn[i] + arrayIn[j];
  	}
  	return result;
  }
}
