package ch.fhnw.richards.other;

public class Factorial {
  // Calculate and print the factorial of all numbers
  // from 1 to 10. Use both int and long.
  // Is there a difference?
  public static void main(String args[]) {
    int int_answer;
    long long_answer;
    for (int i = 1; i <= 21; i++) {
      int_answer = 1;
      long_answer = 1;
      for (int j = 2; j <= i; j++) {
        int_answer *= j;
        long_answer *= j;
      }
      System.out.println(i + "! as int is " + int_answer);
      System.out.println(i + "! as long is " + long_answer);
    }
  }
}