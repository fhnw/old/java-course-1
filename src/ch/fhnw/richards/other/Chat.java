package ch.fhnw.richards.other;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Chat extends JFrame implements ActionListener {
	
	// declare as class variables all GUI items we
	// need to see outside of the constructor
	private JTextField txtComputerName;
	private JTextField txtMessage;
	private JTextArea txtAllMessages;
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	public Chat() {
		// declare as local variables all GUI items
		// we never need to touch after they are created
		JLabel lblComputerName = new JLabel("Computer");
		JLabel lblMessage = new JLabel("Message");
		JButton btnConnect = new JButton("Connect");
		JButton btnSend = new JButton("Send");
		
		// Get a reference to the content pane of our window
		Container contentPane = this.getContentPane();
	}

}
