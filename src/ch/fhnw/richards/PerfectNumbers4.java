package ch.fhnw.richards;

import java.util.Date;
import java.util.Iterator;
import java.util.TreeSet;

public class PerfectNumbers4 {
	static int numPrimeFactors;
	static int numFactors;
	static int maxNumFactors = 0;
	static long[] primeFactors = new long[100];
	static long[] factors = new long[10000];
	static TreeSet<Long> primeNumbers = new TreeSet<Long>();

	public static void main(String[] args) {
		long trying = 1;
		long startTime = new Date().getTime();
		primeNumbers.add((long)2); // easier if we don't start empty...
		for (long num = 2; num <= Long.MAX_VALUE; num++) {
			// tell us what is happening
			if (num > trying) {
				long newTime = new Date().getTime();
				double elapsed = (newTime - startTime) / 1000.0;

				System.out.print("Numbers through " + trying + " (max. factors " + maxNumFactors + ")");
				System.out.println(" - working for " + elapsed + " seconds");
				trying *= 2;
			}

			// System.out.println("Trying " + num);
			primeFactors(num);
			// printlist(primeFactors, numPrimeFactors);
			factors();
			// printlist(factors, numFactors);
			long sum = 0;
			for (int i = 0; i < numFactors; i++) {
				sum += factors[i];
			}
			if (sum == num) {
				System.out.println(sum + " is perfect!");
			}
		}
	}

	// We have processed all numbers up to num - 1. Therefore,
	// we automatically have all prime numbers required to
	// factor num, unless it is itself a prime number.
	private static void primeFactors(long num) {
		numPrimeFactors = 0;
		Iterator<Long> iPrime = primeNumbers.iterator();
		Long prime = iPrime.next();
		while (num > 1) {
			long maxFactor = (long) Math.sqrt(num);
			boolean found = false;
			while (prime <= maxFactor & !found) {
				long div = num / prime;
				if ((div * prime) == num) {
					primeFactors[numPrimeFactors] = prime;
					numPrimeFactors++;
					num = div;
					found = true;
				} else {
					prime = iPrime.next();
				}
			}
			if (!found) {
				primeNumbers.add(num);
				primeFactors[numPrimeFactors] = num;
				numPrimeFactors++;
				num = 1;
			}
		}
	}

	private static void factors() {
		numFactors = 1;
		factors[0] = 1;
		for (int factorNumber = 1; factorNumber < numPrimeFactors; factorNumber++) {
			int[] factorPositions = new int[factorNumber];
			for (int i = 0; i < factorPositions.length; i++) {
				factorPositions[i] = i;
			}
			boolean isNextFactor = true;
			while (isNextFactor) {
				// calculate factor
				long newFactor = 1;
				for (int i = 0; i < factorPositions.length; i++) {
					newFactor *= primeFactors[factorPositions[i]];
				}

				// add factor only if not already present
				boolean found = false;
				for (int i = 0; i < numFactors & !found; i++) {
					long value = factors[i];
					if (value == newFactor) found = true;
				}
				if (!found) {
					factors[numFactors] = newFactor;
					numFactors++;
				}

				// try to increment factor positions
				isNextFactor = false;
				for (int i = factorPositions.length - 1; i >= 0 & !isNextFactor; i--) {
					int maxValue = numPrimeFactors + i - factorPositions.length;
					if (factorPositions[i] < maxValue) {
						// index not yet at max, so increment it
						factorPositions[i]++;

						// reset all following indices
						for (int j = i + 1; j < factorPositions.length; j++) {
							factorPositions[j] = factorPositions[j - 1] + 1;
						}

						// report success
						isNextFactor = true;
					}
				}

			}
		}
		if (numFactors > maxNumFactors) {
			maxNumFactors = numFactors;
		}
	}

	private static void printlist(long[] list, int size) {
		for (int i = 0; i < size; i++) {
			System.out.print(list[i] + ", ");
		}
		System.out.println();
	}

}
