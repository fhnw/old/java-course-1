package ch.fhnw.richards;

import java.io.IOException;

public class BookTest {

	public static void main(String[] args) throws IOException {
		Book[] books = new Book[3];
		int whichBook = 0;
		String titleIn = "xxx";
		String authorIn = "";
		while (titleIn.length() > 0) {
			System.out.print("Enter the title: ");
			titleIn = readline();
			if (titleIn.length() > 0) {
				System.out.print("enter the author: ");
				authorIn = readline();
				Book bookIn = new Book();
				bookIn.setTitle(titleIn);
				bookIn.setAuthor(authorIn);
				books[whichBook++] = bookIn;
			}
		}
		
		for (int i = 0; i < whichBook; i++) {
			System.out.println(books[i].getTitle());
		}
		
		for (int i = 0; i < whichBook; i++) {
			System.out.println(books[i].getAuthor());
		}
	}
	
	// From lecture 3
	public static String readline() throws IOException {
		boolean lineDone = false;
		String stringIn = "";
		
		while (!lineDone) {
			int c = System.in.read();
			lineDone = ( (c == -1) | (c == 13) ); // no data or 'cr'
			if (!lineDone) {
				if (c != 10) { // ignore 'lf'
					stringIn += (char) c;
				}
			}
		}
		return stringIn;
	}

}
