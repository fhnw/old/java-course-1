package ch.fhnw.richards.poker.pokerGraphical1;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

public class PokerHand2 extends Box {
	private JLabel lblContents;
	private JLabel lblValue;
	private JLabel lblScore;
	
	
  public PokerHand2() {
  	super(BoxLayout.Y_AXIS);
  	lblContents = new JLabel("Cards: ");
  	lblValue = new JLabel("Value: ");
  	lblScore = new JLabel("Score: ");

  	this.add(lblContents);
  	this.add(Box.createVerticalStrut(10));
  	this.add(lblValue);
  	this.add(Box.createVerticalStrut(10));
  	this.add(lblScore);
  	this.setBorder(new EmptyBorder(10,10,10,10));
  }
  
}
