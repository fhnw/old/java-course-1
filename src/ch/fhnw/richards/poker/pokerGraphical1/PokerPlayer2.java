package ch.fhnw.richards.poker.pokerGraphical1;

import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

import ch.fhnw.richards.lecture06.poker.poker1.Card;

public class PokerPlayer2 extends Box {
	private JLabel lblName;
	private JLabel lblContents;
	private JLabel lblValue;
	private JLabel lblScore;
	
	private ArrayList<Card> cards;
	private int score; // hands won
	
  public PokerPlayer2(String name) {
  	super(BoxLayout.Y_AXIS);
  	lblName = new JLabel(name);
  	lblContents = new JLabel("Cards: ");
  	lblValue = new JLabel("Value: ");
  	lblScore = new JLabel("Score: 0");

  	this.add(lblName);
  	this.add(Box.createVerticalStrut(10));
  	this.add(lblContents);
  	this.add(Box.createVerticalStrut(10));
  	this.add(lblValue);
  	this.add(Box.createVerticalStrut(10));
  	this.add(lblScore);
  	this.setBorder(new EmptyBorder(10,10,10,10));
  	
  	lblContents.setPreferredSize(new Dimension(300,20));
  	
  	cards = new ArrayList<Card>();
  }
  
	/** Add a card to the hand. Throws an exception
	 * if the hand is already full (five cards)
	 * 
	 * @param Card newCard - the card to add
	 * @return none
	 */
	public void addCard(Card newCard) throws Exception {
		if (cards.size() < 5) {
			cards.add(newCard);
			lblContents.setText(lblContents.getText() + " " + newCard.toString());
		} else {
			throw new Exception("Hand already has five cards!");
		}
	}
	
	/** Determine the value of the hand, by calling the
	 * routines isHighCard, isPair, etc. The value of the
	 * hand will be returned as an integer. The base value
	 * is the value returned by the individual functions,
	 * such as isHighCard. This value is never more than
	 * 100, and is used to break ties between hands of the
	 * same type (e.g., two players both have one pair).
	 * 
	 * For each "level" we a bonus of 100 points to the value of
	 * the hand. So for a pair, we add 100, for 2 pairs,
	 * 200, for three-of-a-kind 300, etc... This differentiates
	 * amongst hands of different types (e.g., three-of-a-kind
	 * always beats one-pair).
	 * 
	 * @param none
	 * @return int - the value of the hand
	 */
	public int valueHand() {
		int bonus;
		int handValue;
		String strValue;
		
		//if (isHighCard() > 0) {
			bonus = 0;
			handValue = isHighCard();
			strValue = "High card " + handValue;
		//}
		
		if (isOnePair() > 0) {
			bonus = 100;
			handValue = isOnePair();
			strValue = "Pair of " + handValue;
		}
		
		if (isTwoPair() > 0) {
			bonus = 300;
			handValue = isTwoPair();
			strValue = "Two pair, highest rank " + handValue;
		}
		
		// And now the rest of the functions...
		
		lblValue.setText(strValue);
		return bonus + handValue;
	}
	
	/** Record the fact that this player wins a hand
	 * 
	 * @param none
	 * @return none
	 */
	public void wins() {
		score += 1;
		lblScore.setText("Score: " + Integer.toString(score));
	}
	
	/** Discard the player's hand, to get ready for
	 * the next round.
	 * 
	 * @param none
	 * @return none
	 */
	public void discardHand() {
		cards = new ArrayList<Card>();
		lblContents.setText("");
		lblValue.setText("");
	}
	
	/** A poker hand as a string ist the contents
	 * of each card (or "null" for missing cards)
	 * @param none
	 * @return String - the cards in the hand
	 */
	public String toString() {
		String out = "Hand contains:";
		for (int i = 0; i < cards.size(); i++) {
			out += " " + cards.get(i).toString();
		}
		return out;
	}
	
	/** Check to see if the hand meets the criteria for "high card"
	 * (Note: <i>every</i> hand does!). If so, return the rank of
	 * the highest card in the hand. This method can be used as a
	 * skeleton for the more complex methods such as isPair
	 * 
	 * @param none
	 * @return int - the rank of the highest card in the hand
	 */
	public int isHighCard() {
		boolean meetsCriteria = false;
		int returnValue = 0;
		
		// Test to see if the criteria are met.
		meetsCriteria = true; // For "high card" very simple
		
		// Find the value to return
		if (meetsCriteria) {
			for( Card c : cards ) {
				if (c.getRank() > returnValue) {
					returnValue = c.getRank();
				}
			}
		}
		
		// Return the value
		return returnValue;
	}
	
	/** Check to see if the hand meets the criteria for "one pair"
	 * If so, return the rank of the pair. Note that this method
	 * will also recognize three-of-a-kind, full house, etc. It
	 * returns the rank of the first pair it finds.
	 * 
	 * @param none
	 * @return int - the rank of the pair found
	 */
	public int isOnePair() {
		boolean meetsCriteria = false;
		int returnValue = 0;
		
		// Test to see if the criteria are met.
		for ( int i = 0; i < (cards.size() - 1); i++ ) {
			for ( int j = i+1; j < cards.size(); j++) {
				if (cards.get(i).getRank() == cards.get(j).getRank()) {
					returnValue = cards.get(i).getRank();
					meetsCriteria = true;
					break;
				}
			}
			if (meetsCriteria) break;
		}
		
		// Return the value
		return returnValue;
	}
	
	/** Check to see if the hand meets the criteria for "two pair"
	 * If so, return the rank of the highest pair.
	 * 
	 * Note: this method was added in 2009, as part of the final exam
	 * 
	 * @param none
	 * @return int - the rank of the pair found
	 */
	public int isTwoPair() {
		// Create an array with one element for each rank
		int[] cardRanks = new int[13];
		
		// Count the number of each rank in our hand
		for ( Card card : cards ) {
			int rank = card.getRank();
			cardRanks[rank-2]++;
		}

		// See if we have two pairs
		int pairsFound = 0;
		int highestPairRank = 0;
		for (int rank = 2; rank <= 14; rank++) {
			if (cardRanks[rank-2] == 2) {
				highestPairRank = rank;
				pairsFound++;
			}
		}

		if (pairsFound >= 2) {
			return highestPairRank;
		} else {
			return 0;
		}
	}

	/** Return the number of cards currently in the hand
	 * @param none
	 * @return int - the number of cards in the hand
	 */
	public int getCardsInHand() {
		return cards.size();
	}
}
