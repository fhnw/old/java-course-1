package ch.fhnw.richards.poker.pokerGraphical1;

import java.util.ArrayList;
import javax.swing.JLabel;

import ch.fhnw.richards.lecture06.poker.poker1.Card;

/** This class is used in lecture 6 of the first semester
 * Java programming course.
 * 
 * This class represents a deck of cards. Upon creation, all
 * cards are present in the deck. As the cards are dealt, they
 * are removed. Dealt cards are removed from the array and
 * replaced by null.
 *
 * Since we don't yet know about enumerated types, suit and
 * rank are represented as integers.
 * 
 * @author Brad Richards
 *
 */
public class DeckOfCards2 extends JLabel {
	private ArrayList<Card> cards;

	/** Create a fresh deck of cards
   *
   */
	public DeckOfCards2(){
		shuffle();
	}
	
	/** Return the number of cards remaining in the deck
	 * @param none
	 * @return int - the number of cards remaining (0 to 52)
	 */
	public int getCardsRemaining() {
		return cards.size();
	}
	
	/** Deal one of the cards remaining in the deck. This is
	 * done simply: generate a random number 0-x, where x+1 is
	 * the number of cards remaining in the deck.
	 * 
	 * @param none
	 * @return Card - one of the cards remaining in the deck
	 */
	public Card dealCard() throws Exception {
		int cardNumber;
		Card cardToDeal = null;
		if (cards.size() <= 0) {
			throw new Exception("No card left!");
		} else {
			cardNumber = (int) (cards.size() * java.lang.Math.random());
			cardToDeal = cards.remove(cardNumber);
		}
		this.setText(cards.size() + " cards");
		return cardToDeal;
	}
	
	/** Re-initialize the deck
	 * 
	 * @param none
	 * @return none
	 */
	public void shuffle() {
		cards = new ArrayList<Card>();
		for (int suit = 1; suit <= 4; suit++) {
			for (int rank = 2; rank <= 14; rank++) {
				Card newCard = new Card(suit, rank);
				cards.add(newCard);
			}
		}
		this.setText(cards.size() + " cards");
	}
}
