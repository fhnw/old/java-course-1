package ch.fhnw.richards.poker.pokerGraphical1;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import ch.fhnw.richards.lecture06.poker.poker1.Card;

public class PokerGame2 extends JFrame {
	private DeckOfCards2 deck;
	private PokerPlayer2 player1;
	private PokerPlayer2 player2;

	private JButton btnDeal;
	private JButton btnShuffle;

	public static void main(String[] args) {
		new PokerGame2();
	}

	public PokerGame2() {
		super("Poker");

		deck = new DeckOfCards2();
		player1 = new PokerPlayer2("Player 1");
		player2 = new PokerPlayer2("Player 2");

		// topBox can be declared locally,
		// since we never need to access it again...
		Box topBox = Box.createHorizontalBox();
		btnDeal = new JButton("Deal hands");
		btnShuffle = new JButton("Shuffle cards");
		topBox.add(deck);
		topBox.add(Box.createHorizontalStrut(10));
		topBox.add(btnDeal);
		topBox.add(Box.createHorizontalStrut(10));
		topBox.add(btnShuffle);
		topBox.setBorder(new EmptyBorder(10, 10, 10, 10));

		this.setLayout(new BorderLayout());
		this.add(topBox, BorderLayout.NORTH);
		this.add(player1, BorderLayout.WEST);
		this.add(player2, BorderLayout.EAST);

		this.pack();
		this.setVisible(true);

		btnDeal.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				dealCards();
			}
		});
		btnShuffle.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				shuffle();
			}
		});
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent evt) {
				System.exit(0); // JVM beenden
			}
		});

		shuffle();
	}

	private void shuffle() {
		player1.discardHand();
		player2.discardHand();
		deck.shuffle();
	}

	/**
	 * Deal the cards, five to each hand
	 * 
	 * @params none
	 * @return none
	 */
	private void dealCards() {
		player1.discardHand();
		player2.discardHand();
		try {
			for (int i = 0; i < 5; i++) {
				Card c = deck.dealCard();
				player1.addCard(c);
				c = deck.dealCard();
				player2.addCard(c);
			}
			
		int p1 = player1.valueHand();
		int p2 = player2.valueHand();
		if (p1 > p2) {
			player1.wins();
		} else if (p2 > p1) {
			player2.wins();
		}
		
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}
