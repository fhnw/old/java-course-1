package ch.fhnw.richards.poker.pokerGraphical2;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class PokerGame3  extends JPanel {
		private PokerDeck deck;
		private PokerPlayer3 player1;
		private PokerPlayer3 player2;

		private JButton btnDeal;
		private JButton btnShuffle;
		
		private Container parent;

		public PokerGame3(Container parent) {
			this.parent = parent;
			deck = new PokerDeck();
			player1 = new PokerPlayer3("Player 1");
			player2 = new PokerPlayer3("Player 2");

			// topBox can be declared locally,
			// since we never need to access it again...
			Box topBox = Box.createHorizontalBox();
			btnDeal = new JButton("Deal hands");
			btnShuffle = new JButton("Shuffle cards");
			topBox.add(deck);
			topBox.add(Box.createHorizontalStrut(10));
			topBox.add(btnDeal);
			topBox.add(Box.createHorizontalStrut(10));
			topBox.add(btnShuffle);
			topBox.setBorder(new EmptyBorder(10, 10, 10, 10));

			this.setLayout(new BorderLayout());
			this.add(topBox, BorderLayout.NORTH);
			this.add(player1, BorderLayout.WEST);
			this.add(player2, BorderLayout.EAST);

			btnDeal.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent evt) {
					dealCards();
				}
			});
			btnShuffle.addMouseListener(new MouseAdapter() {
				public void mouseClicked(MouseEvent evt) {
					shuffle();
				}
			});

			shuffle();
		}

		private void shuffle() {
			player1.discardHand();
			player2.discardHand();
			deck.shuffle();
		}

		/**
		 * Deal the cards, five to each hand
		 * 
		 * @params none
		 * @return none
		 */
		private void dealCards() {
			player1.discardHand();
			player2.discardHand();
			try {
				for (int i = 0; i < 5; i++) {
					DeckOfCards3.Card c = deck.dealCard();
					player1.addCard(c);
					c = deck.dealCard();
					player2.addCard(c);
				}
				
			int p1 = player1.valueHand();
			int p2 = player2.valueHand();
			if (p1 > p2) {
				player1.wins();
			} else if (p2 > p1) {
				player2.wins();
			}
			
			} catch (Exception e) {
				System.out.println(e.toString());
			}
		}
	}
