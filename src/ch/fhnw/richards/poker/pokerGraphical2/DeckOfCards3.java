package ch.fhnw.richards.poker.pokerGraphical2;

import java.util.ArrayList;

import javax.swing.JLabel;

/**
 * This class is used in lecture 6 of the first semester Java programming course.
 * 
 * This class represents a deck of cards. Upon creation, all cards are present in the deck. As the
 * cards are dealt, they are removed. Dealt cards are removed from the array and replaced by null.
 * 
 * Since we don't yet know about enumerated types, suit and rank are represented as integers.
 * 
 * @author Brad Richards
 * 
 */
public abstract class DeckOfCards3 extends JLabel {
	private int minRank = 0;
	private int maxRank = 0;

	private ArrayList<Card> cards;

	/**
	 * Create a fresh deck of cards
	 * 
	 */
	public DeckOfCards3() {
	}

	/**
	 * Return the number of cards remaining in the deck
	 * 
	 * @param none
	 * @return int - the number of cards remaining (0 to 52)
	 */
	public int getCardsRemaining() {
		return cards.size();
	}

	/**
	 * Deal one of the cards remaining in the deck. This is done simply: generate a random number 0-x,
	 * where x+1 is the number of cards remaining in the deck.
	 * 
	 * @param none
	 * @return Card - one of the cards remaining in the deck
	 */
	public Card dealCard() throws Exception {
		int cardNumber;
		Card cardToDeal = null;
		if (cards.size() <= 0) {
			throw new Exception("No card left!");
		} else {
			cardNumber = (int) (cards.size() * java.lang.Math.random());
			cardToDeal = cards.remove(cardNumber);
		}
		this.setText(cards.size() + " cards");
		return cardToDeal;
	}

	/**
	 * Re-initialize the deck
	 * 
	 * @param none
	 * @return none
	 */
	public void shuffle() {
		cards = new ArrayList<Card>();
		for (int suit = 1; suit <= 4; suit++) {
			for (int rank = minRank; rank <= maxRank; rank++) {
				Card newCard = null;
				try {
					newCard = new Card(suit, rank);
				} catch (Exception e) {
					// can never happen
				}
				cards.add(newCard);
			}
		}
		this.setText(cards.size() + " cards");
	}

	void setMinRank(int rank) {
		minRank = rank;
	}	
	public int getMinRank() {
		return minRank;
	}
	void setMaxRank(int rank) {
		maxRank = rank;
	}
	int GetMaxRank() {
		return maxRank;
	}
	
	/**
	 * This class is used in lecture 6 of the first semester Java programming course.
	 * 
	 * This class represents a single card from a deck of cards.
	 * 
	 * Since we don't yet know about enumerated types, suit and rank are represented as integers.
	 * 
	 * @author Brad Richards
	 * 
	 */
	public class Card {
		private int rank;
		private int suit;

		/**
		 * Create a card of the given rank and suit
		 * 
		 * @param suit -
		 *          the suit of the card (1-4)
		 * @param rank -
		 *          the rank of the card (2-14)
		 * @throws Exception 
		 */
		public Card(int suit, int rank) throws Exception {
			// Check with the current deck, to be sure this card is possible
			if ((rank >= minRank) & (rank <= maxRank)) {
				this.rank = rank;
				this.suit = suit;
			} else {
				throw new Exception("invalid card");
			}
		}

		/**
		 * Return a string representation of this card
		 * 
		 * @param none
		 * @return String in the form of <suit, rank>
		 */
		public String toString() {
			return "<" + suit + ", " + rank + ">";
		}

		/**
		 * Return the suit of the card
		 * 
		 * @return int - the suit of the card
		 */
		public int getSuit() {
			return suit;
		}

		/**
		 * Return the rank of the card
		 * 
		 * @return int - the rank of the card
		 */
		public int getRank() {
			return rank;
		}
			}
}
