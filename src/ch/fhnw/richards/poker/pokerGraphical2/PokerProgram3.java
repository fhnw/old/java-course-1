package ch.fhnw.richards.poker.pokerGraphical2;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.WindowConstants;


public class PokerProgram3 extends JFrame {
	private PokerGame3 game;

	public static void main(String[] args) {
		new PokerProgram3();
	}

	public PokerProgram3() {
		setTitle("Poker Game 3");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		game=new PokerGame3(this);
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}
}
