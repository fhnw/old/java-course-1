package ch.fhnw.richards.poker.pokerGraphical2;

import java.util.ArrayList;

import javax.swing.JLabel;


public class PokerDeck extends DeckOfCards3 {

	public PokerDeck() {
		super();
		setMinRank(2);
		setMaxRank(14);
		shuffle();
	}
}