package ch.fhnw.richards.poker.pokerGraphical2;

import java.awt.BorderLayout;

import javax.swing.JApplet;


public class PokerApplet extends JApplet {
	private PokerGame3 game;
	
	public PokerApplet() {
		game = new PokerGame3(this);
		this.setLayout(new BorderLayout());
		this.add(game, BorderLayout.CENTER);
	}
}
