package ch.fhnw.richards;

public class FloatBeispiel {

	public static void main(String[] args) {
		float a = 1000000;
		float b = (a+1)/1000;
		float c = a/1000;
		float d = b-c;
		float e = 1000000 * d;
		float f = e-1000;
		
		System.out.println("Antwort soll 0 sein, ist aber " + f);
	}

}
