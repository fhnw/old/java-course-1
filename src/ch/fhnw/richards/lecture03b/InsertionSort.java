package ch.fhnw.richards.lecture03b;
import java.util.logging.*;

public class InsertionSort {
	private static Logger logger = Logger.getLogger("ch.fhnw.richards");
	public static void main(String[] args) {
		int[] intArgs = new int[args.length];
		logger.setLevel(Level.INFO); // Set desired level

		// Fill the array
		for (int i = 0; i < args.length; i++) {
			logger.info("Processing argument '" + args[i] + "'");
			intArgs[i] = Integer.parseInt(args[i]);
		}
		
		// Sort the array: using an insertion sort into a new
		// array. Grab the next element, find the spot in the
		// new array, and insert it there, shoving current
		// elements to the right.
		int[] sortedArgs = new int[args.length];
		int elementsMoved = 0; 
		int element, shouldBe;
		
		// for each element in the original array
		for (int i = 0; i < args.length; i++) {
			element = intArgs[i];
			logger.info("arg[" + i + "] is " + element);
			
			// Find the spot where this element belongs in the new array.
			// Beginning at zero, look until we find a larger element.
			shouldBe = 0;
			while (shouldBe < elementsMoved & sortedArgs[shouldBe] < element) {
				shouldBe++;
			}
			logger.info("Element belongs at position " + shouldBe);

			// Before we can put this element where it goes, we
			// have to shove all current elements to the right
			// Question: why do we use a count-down loop?
			for (int j = elementsMoved - 1; j >= shouldBe; j--) {
				assert ( j >= 0 & j < (sortedArgs.length-1));
				sortedArgs[j+1] = sortedArgs[j];
			}
			
			// Put the element where it goes
			sortedArgs[shouldBe] = element;
			elementsMoved++;
		}

		// Print array
		System.out.print("The result is: [");
		for (int i : sortedArgs) {
			System.out.print(i + ", ");
		}
		System.out.println("]");
	}
}
