package ch.fhnw.richards.lecture03b;

import java.util.Scanner;

public class LetterFrequencyAnalysis {

	public static void main(String[] args) {
		// Read a maximum of 100 strings from the console
		String[] stringsToAnalyze = readInputStrings(100);
		
		// Transform the strings into characters
		char[][] charsToCount = transformStrings(stringsToAnalyze);
		
		// Count the characters
		int[] letterFrequencies = countCharacters(charsToCount);

		// Print the results in a nice format
		printResults(letterFrequencies);
	}

	/**
	 * Read input from the console, until a control-D is entered,
	 * or until we have read the maximum number of strings.
	 * 
	 * @param maxNumStrings the maximum number of string we will read
	 * @return An array of strings
	 */
	private static String[] readInputStrings(int maxNumStrings) {
		String[] stringsIn = new String[maxNumStrings];
		int stringsEntered = 0;
		Scanner s = new Scanner(System.in);
		
		while (s.hasNext() & stringsEntered < stringsIn.length) {
			stringsIn[stringsEntered++] = s.next();
		}

		// We could just return the array of String, but why not
		// create a new one of the correct length?
		String[] stringsOut = new String[stringsEntered];
		for (int i = 0; i < stringsEntered; i++) {
			stringsOut[i] = stringsIn[i];
		}
		
		return stringsOut;
	}
	
	/**
	 * Transform an array of strings into a 2-dimensional character
	 * array
	 * 
	 * @param stringsIn the strings to be processed
	 * @return A 2-dimensional character array
	 */
	private static char[][] transformStrings(String[] stringsIn) {
		char[][] charsOut = new char[stringsIn.length][]; // The second dimension will vary...
		for (int i = 0; i < stringsIn.length; i++) {
			charsOut[i] = stringsIn[i].toCharArray(); // Create one row in the output array
		}
		return charsOut;
	}
	
	/**
	 * Count the occurences of the letters a-z, placing the result in an
	 * array int[26]. Disregard any other characters that we see.
	 * 
	 * @param charsIn the characters to be counted
	 * @return An int array containing the frequencies of the letters a-z
	 */
	private static int[] countCharacters(char[][] charsIn) {
		int[] letterFrequencies = new int[26]; // Elements are automatically zero
		for (char[] arrayRow : charsIn) {
			for (char c : arrayRow) {
				if (c >= 'a' & c <= 'z') {
					int index = c - 'a';
					letterFrequencies[index]++;
				}
			}
		}
		return letterFrequencies;
	}
	
	/**
	 * Print the frequencies of the letters
	 * 
	 * @param frequencies: a 26-place int array giving the frequencies of the letters a-z
	 */
	private static void printResults(int[] frequencies) {
		// Print the header, allowing space for 3-digit frequencies
		for (char c = 'a'; c <= 'z'; c++) {
			System.out.print("   " + c);
		}
		System.out.println();
		
		// Print the frequency array, adding spaces for alignment
		for (int f : frequencies) {
			if (f < 100) System.out.print(" ");
			if (f < 10) System.out.print(" ");
			System.out.print(" " + f);
		}
		System.out.println();
	}
}
