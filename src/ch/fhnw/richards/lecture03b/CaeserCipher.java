package ch.fhnw.richards.lecture03b;

import java.util.Scanner;

/**
 * This program implements the Caeser cipher. It reads the unencoded string and the key from the
 * console, and writes the encoded string to the console.
 * 
 * @author brad
 */
public class CaeserCipher {

	public static void main(String[] args) {
		String inputString;
		String outputString;
		int encodingKey;

		// Read input string and encoding key
		Scanner s = new Scanner(System.in);
		System.out.print("Enter input string: ");
		inputString = s.next();
		System.out.print("Enter encoding key: ");
		encodingKey = s.nextInt();

		// Convert input string to array of characters
		char[] inputChars = inputString.toCharArray();

		// Create array of characters for output
		char[] outputChars = new char[inputChars.length];

		// Process each character: input --> output
		for (int i = 0; i < inputChars.length; i++) {
			outputChars[i] = encodeCharacter(inputChars[i], encodingKey);
		}

		// Convert the output char-array to a string
		outputString = new String(outputChars);

		// Print the encoded string
		System.out.println("The encoded string is '" + outputString + "'");
	}

	/**
	 * Encode a single character using the Caeser cipher. This method assumes that the character is a
	 * letter. Upper-case and lower-case letters are encoded separately. The results are undefined for
	 * all other inputs.
	 * 
	 * @param in
	 *          - the character to encode
	 * @param key
	 *          - the key (offset) for the cipher
	 * @return - the encoded character
	 */
	private static char encodeCharacter(char in, int key) {
		int outInt = in + key;
		char out = (char) outInt;

		// If encoded character is past the end of the alphabet,
		// shift it back to the beginning.
		if ((in <= 'Z' & out > 'Z') | (in <= 'z' & out > 'z')) {
			outInt -= 26;
			out = (char) outInt;
		}

		return out;
	}
}
