package ch.fhnw.richards.lecture03b;

/**
 * ProjectEuler problem 10, generating primes using the Sieve of Erastothenes
 * 
 * @author brad
 * 
 */
public class Euler20 {
	private static boolean[] primes;
	private static final int LENGTH = 2000000; // constant

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		long sum; // cannot be int - result will be too large!

		// Create and initialize the boolean array with 1000000 elements
		primes = new boolean[LENGTH];
		primes[0] = false;
		primes[1] = false;
		// for (int i = 2; i < LENGTH; i++) {
		// primes[i] = true;
		// }
		for (boolean p : primes)
			p = true;

		// Call method to mark primes in the array (Sieve of Erastothenes)
		sieveOfErastothenes();

		// Call method to sum primes through 2000000
		sum = sumPrimes();

		// Print result
		System.out.println("The result is " + sum);
	}

	private static void sieveOfErastothenes() {
		// For all elements from 2 through sqrt(LENGTH)
		for (int i = 2; i < Math.sqrt(LENGTH); i++) { // see java.lang.Math
			// If element x is prime
			if (primes[i]) {
				// For all multiples from x-squared through LENGTH
				for (int j = i * i; j < LENGTH; j += i) {
					// Mark as non-prime
					primes[j] = false;
				}
			}
		}
	}

	/**
	 * Calculate the sum of the primes in the array of primes
	 * 
	 * @return (long) sum of the primes
	 */
	private static long sumPrimes() {
		long sum = 0;
		for (int i = 2; i < LENGTH; i++) {
			if (primes[i]) {
				sum += i;
			}
		}
		return sum;
	}
}
