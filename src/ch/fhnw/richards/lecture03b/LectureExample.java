package ch.fhnw.richards.lecture03b;

public class LectureExample {
	public static void main(String[] args) {
		int[] result;
		int[] intArgs = new int[args.length];

		for (int i = 0; i < args.length; i++) {
			intArgs[i] = Integer.parseInt(args[i]);
		}
		result = processArray(intArgs);
		System.out.print("The result is: [");
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i] + ", ");
		}
		System.out.println("]");
	}

	public static int[] processArray(int[] arrayIn) {
		int numNewElements = (arrayIn.length + 1) / 2;
		int[] result = new int[numNewElements];
		for (int i = 0; i < numNewElements; i++) {
			result[i] = arrayIn[i] + arrayIn[arrayIn.length - i];
		}
		return result;
	}
}
