package ch.fhnw.richards.lecture03b;
import java.util.logging.*;

public class SelectionSort {
	private static Logger logger = Logger.getLogger("ch.fhnw.richards");
	public static void main(String[] args) {
		int[] intArgs = new int[args.length];
		logger.setLevel(Level.OFF); // Set desired level

		// Fill the array
		for (int i = 0; i < args.length; i++) {
			logger.info("Processing argument '" + args[i] + "'");
			intArgs[i] = Integer.parseInt(args[i]);
		}
		
		// Sort the array: using an in-place selection sort
		// Repeat: find smallest element and swap to front
		for (int i = 0; i < (args.length-1); i++) {
			logger.info("i = " + i);
			int smallest = i;
			for (int j = i+1; j < args.length; j++) {
				logger.info("j = " + j);
				if (intArgs[smallest] > intArgs[j]) {
					// found a smaller element
					smallest = j;
				}
			}
			logger.info("smallest is position " + smallest + " with value " + intArgs[smallest]);
			if (smallest != i) {
				logger.info("swapping elements " + i + " and " + smallest);
				// swap smallest to front
				int tmp = intArgs[i];
				intArgs[i] = intArgs[smallest];
				intArgs[smallest] = tmp;
			}
		}

		// Print array
		System.out.print("The result is: [");
		for (int i : intArgs) {
			System.out.print(i + ", ");
		}
		System.out.println("]");
	}
}
