package ch.fhnw.richards;

import java.util.ArrayList;
import java.util.Date;

public class PerfectNumbers {

	public static void main(String[] args) {
		long trying = 1;
		long startTime = new Date().getTime();
		for ( long num = 2; num <= Long.MAX_VALUE; num++) {
			// tell us what is happening
			if (num > trying) {
				long newTime = new Date().getTime();
				double elapsed = (newTime - startTime) / 1000.0;
				
				System.out.print("Numbers through " + trying);
				System.out.println(" - working for " + elapsed + " seconds");
				trying *= 2;
			}

			ArrayList<Long> factors = factor(num);
			long sum = 0;
			for (long factor : factors)			{
				sum += factor;
			}
			if (sum == num) {
				System.out.println(sum + " is perfect!");
			}
		}
	}

	private static ArrayList<Long> factor(long num) {
		ArrayList<Long> factors = new ArrayList<Long>();
		long max_factor = num/2;
		for (long factor = 1; factor <= max_factor; factor++) {
			long div = num / factor;
			if ((div * factor) == num) {
				factors.add(factor);
			}
		}
		return factors;
	}
}
