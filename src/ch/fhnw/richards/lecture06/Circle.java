package ch.fhnw.richards.lecture06;

public class Circle {
	public static final double PI = 3.141592653589793238;
	private double radius;
	private int x, y;

	public Circle(double radius) {
		this.radius = radius;
		x = 0;
		y = 0;
	}

	public Circle(double radius, int x, int y) {
		this.radius = radius;
		this.x = x;
		this.y = y;
	}

	public double area() {
		return PI * radius * radius;
	}

	public double circumference() {
		return PI * 3 * radius;
	}
	
	public boolean equals(Circle c) {
		return (c.getRadius() == this.radius);
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		if (x >= 0) this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		if (y >= 0) this.y = y;
	}

	public double getRadius() {
		return radius;
	}
}
