package ch.fhnw.richards.lecture06.poker.poker1;


/** This class is used in lecture 6 of the first semester
 * Java programming course.
 * 
 * This class represents a poker hand of five cards.
 * 
 * @see Card
 * @see DeckOfCards
 *  
 * @author Brad Richards
 *
 */
public class PokerHand {
	private Card[] cards;
	private int cardsInHand = 0;
	
	/** Create an empty hand
	 * @param none
	 */
	public PokerHand(){
		cards = new Card[5];
	}
	
	/** Add a card to the hand. Throws an exception
	 * if the hand is already full (five cards)
	 * 
	 * @param Card newCard the card to add
	 * @return none
	 */
	public void addCard(Card newCard) throws Exception {
		if (cardsInHand < 5) {
			cards[cardsInHand] = newCard;
			cardsInHand += 1;
		} else {
			throw new Exception("Hand already has five cards!");
		}
	}
	
	/** A poker hand as a string is the contents
	 * of each card (or "null" for missing cards)
	 * @param none
	 * @return String the cards in the hand
	 */
	public String toString() {
		String out = "Hand contains:";
		for (Card c : cards) {
			out += " ";
			if (c == null) {
				out += "null";
			} else {
				out += c.toString();
			}
		}
		return out;
	}
}
