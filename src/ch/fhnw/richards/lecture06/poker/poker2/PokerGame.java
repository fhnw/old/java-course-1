package ch.fhnw.richards.lecture06.poker.poker2;

/** Represents a game of poker between two players.
 * The game consists of a single hand.
 * 
 * @author Brad Richards
 */
public class PokerGame {
	private DeckOfCards deck;
	private PokerHand hand1;
	private PokerHand hand2;
	
	/**
	 * Create a poker game
	 * 
	 * @param args - command line arguments, not used
	 */
	public static void main(String[] args) throws Exception {
		PokerGame game = new PokerGame();
		game.runGame();
	}

	/** Create a new game
	 * @params none
	 */
	private PokerGame() {
		deck = new DeckOfCards();
		hand1 = new PokerHand();
		hand2 = new PokerHand();
	}
	
	/** Run the game:
	 * <ol>
	 * <li>Deal each player a PokerHand</li>
	 * <li>Print the hands to the console</li>
	 * <li>Compare the hands and declare the winner</li>
	 * </ol>
	 * 
	 * @params none
	 * @return none
	 */
	private void runGame() throws Exception {
		this.dealCards();
		this.printHands();
		this.findWinner();
	}
	
	/** Deal the cards, five to each hand
	 * 
	 * @params none
	 * @return none
	 */
	private void dealCards() throws Exception {
		for (int i = 0; i < 5; i++) {
			Card c = deck.dealCard();
			hand1.addCard(c);
			c = deck.dealCard();
			hand2.addCard(c);
		}
	}
	
	/** Print the hands
	 * 
	 * @params none
	 * @return none
	 */
	private void printHands() {
		System.out.println("\nHands dealt");
		System.out.println(hand1.toString());
		System.out.println(hand2.toString());
	}
	
	/** Compare the hands, print what each hand has (e.g., pair,
	 * full-house, flush, straight, etc.), and declare a winner.
	 * 
	 * <b><i>Note: this method is not complete - completing it is part
	 * of the exercises for Lecture 6!</i></b>
	 * 
	 * @params none
	 * @return none
	 */
	private void findWinner() {
		System.out.println("\nEvaluating hands");
		int compareResult = hand1.compareTo(hand2);
		if (compareResult < 0) {
			System.out.println("Hand 2 wins!");
		} else if (compareResult > 0) {
			System.out.println("Hand 1 wins!");
		} else {
			System.out.println("Split the pot!");
		}
	}
}
