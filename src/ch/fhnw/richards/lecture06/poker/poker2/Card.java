package ch.fhnw.richards.lecture06.poker.poker2;

/** This class is used in lecture 6 of the first semester
 * Java programming course.
 * 
 * This class represents a single card from a deck of cards.
 *
 * Since we don't yet know about enumerated types, suit and
 * rank are represented as integers.
 * 
 * @author Brad Richards
 *
 */
public class Card {
	private int rank;
	private int suit;
  /** Create a card of the given rank and suit
   * @param suit - the suit of the card (1-4)
   * @param rank - the rank of the card (2-14)
   */
	public Card(int suit, int rank) {
		this.rank = rank;
		this.suit = suit;
	}
	
	/** Return a string representation of this card
	 * @param none
	 * @return String in the form of <suit, rank>
	 */
	public String toString() {
		return "<" + suit + ", " + rank + ">";
	}	
	
	/** Return the suit of the card
	 * @return int - the suit of the card
	 */
	public int getSuit() {
		return suit;
	}
	
	/** Return the rank of the card
	 * @return int - the rank of the card
	 */
	public int getRank() {
		return rank;
	}
}
