package ch.fhnw.richards.lecture06.poker.poker2;

/** This class is used in lecture 6 of the first semester
 * Java programming course.
 * 
 * This class represents a poker hand of five cards.
 * 
 * @see Card
 * @see DeckOfCards
 *  
 * @author Brad Richards
 *
 */
public class PokerHand implements Comparable<PokerHand> {
	private Card[] cards;
	private int cardsInHand = 0;
	
	/** Create an empty hand
	 * @param none
	 */
	public PokerHand(){
		cards = new Card[5];
	}
	
	/** Add a card to the hand. Throws an exception
	 * if the hand is already full (five cards)
	 * 
	 * @param Card newCard the card to add
	 * @return none
	 */
	public void addCard(Card newCard) throws Exception {
		if (cardsInHand < 5) {
			cards[cardsInHand] = newCard;
			cardsInHand += 1;
		} else {
			throw new Exception("Hand already has five cards!");
		}
	}
	
	/** A poker hand as a string is the contents
	 * of each card (or "null" for missing cards)
	 * @param none
	 * @return String the cards in the hand
	 */
	public String toString() {
		String out = "Hand contains:";
		for (Card c : cards) {
			out += " ";
			if (c == null) {
				out += "null";
			} else {
				out += c.toString();
			}
		}
		return out;
	}
	
	@Override
	public int compareTo(PokerHand o) {
		int thisValue = this.getValuation();
		int otherValue = o.getValuation();
		int compareValue = thisValue - otherValue;
		return compareValue;
	}
	
	private int getValuation() {
		int valuation = 0; // Every hand has a high card
		if (this.isPair()) valuation = 1;
		if (this.isTwoPair()) valuation = 2;
		if (this.isThreeOfAKind()) valuation = 3;
		// And so on for other types of hands
		return valuation;
	}
	
	private boolean isPair() {
		boolean foundPair = false;
		// Look for two cards of the same rank
		for (int i = 0; i < 4; i++) {
			for (int j = i+1; j < 5; j++) {
				if (cards[i].getRank() == cards[j].getRank()) foundPair = true;
			}
		}		
		return foundPair;
	}
	
	private boolean isTwoPair() {
		return false;
	}
	
	private boolean isThreeOfAKind() {
		return false;
	}
}
