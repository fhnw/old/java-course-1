package ch.fhnw.richards.lecture06.poker.poker2;


/**  
 * This class represents a deck of cards. Upon creation, all
 * cards are present in the deck. As the cards are dealt, they
 * are removed. Dealt cards are removed from the array and
 * replaced by null.
 *
 * Since we don't yet know about enumerated types, suit and
 * rank are represented as integers.
 * 
 * @author Brad Richards
 *
 */
public class DeckOfCards {
	private Card[] cards;
	private int cardsRemaining = 0;
	
  /**
   * Create a fresh deck of cards
   */
	public DeckOfCards(){
		int cardNumber = 0;
		cards = new Card[52];
		for (int suit = 1; suit <= 4; suit++) {
			for (int rank = 2; rank <= 14; rank++) {
				Card newCard = new Card(suit, rank);
				assert(cardNumber < 52);
				cards[cardNumber] = newCard;
				cardNumber++;
			}
		}
		cardsRemaining = 52;
	}
	
	/** Return the number of cards remaining in the deck
	 * @param none
	 * @return int the number of cards remaining (0 to 52)
	 */
	public int getCardsRemaining() {
		return cardsRemaining;
	}
	
	/** Deal one of the cards remaining in the deck. This is
	 * done simply: generate a random number 0-51, until we
	 * find a non-null entry in the array.
	 * 
	 * @param none
	 * @return Card one of the cards remaining in the deck
	 */
	public Card dealCard() throws Exception {
		int cardNumber;
		Card cardToDeal = null;
		if (cardsRemaining <= 0) {
			throw new Exception("No card left!");
		} else {
			while ( cardToDeal == null) {
				cardNumber = (int) (52.0 * java.lang.Math.random());
				if (cards[cardNumber] != null) {
					cardToDeal = cards[cardNumber];
					cards[cardNumber] = null;
					cardsRemaining -= 1;
				}
			}
		}
		return cardToDeal;
	}
}
